Neon
====

The Neon Engine is a light-weight Ruby game engine which provides 3D audio and rendering via FFI and OpenGL and OpenAL.

Please be aware that as the engine is currently in an experimental development phase, not all features will be available or work similarly on all platforms and runtimes.

## Development and Usage Dependencies

Please ensure your Ruby of choice is at least version 2.0 (currently testing on v2.2.2) for full support of keyword arguments, then install the following gems and dependencies:

* `chunky_png` for PNG image decoding
  * Use `oily_png` if you want even faster processing (C-ext support **required**)
* `json` to manage configuration files
* `msgpack` for binary asset schema
* system-compatible OpenGL, OpenAL, and GLFW 3 libraries

### [!] Important
Your Ruby version **must** support [Fiddle](http://ruby-doc.org/stdlib-2.2.3/libdoc/fiddle/rdoc/Fiddle.html). In most cases, you need not worry; Fiddle is part of Ruby's stdlib and will ship with most Ruby versions.

As previously stated, your version of Ruby should be compatible with version 2.0 or later, as many of the classes and features of the engine rely on and exploit the nature of Keyword Arguments, also known as named parameters, which were added in Ruby 2.

## OpenGL Support

The engine includes a lightweight wrapper to OpenGL with support for 4.5 and ARB extensions.  It is built on Fiddle and uses the official OpenGL headers as reference.  The version of OpenGL available will vary based on your graphics drivers.  Please ensure your drivers are the most current version available from your vendor for your chipset or card.

Microsoft Windows is distributed with `Opengl32.dll`; Mac OSX and later should have `libGL.dylib`.

Linux users may need to install a compatible GL module using their package manager of choice.  Regardless of which is chosen, it should provide `libGL.so`.

To make use of GLFW for application windowing, please ensure you have the [latest version of GLFW 3](http://www.glfw.org/download.html) installed.  At the termination of runtime, the GLFW window will be automatically cleaned up.

#### Linux
Linux users will be able to install the following:

**Debian** or .deb based repos:
```sh
sudo apt-get install libglfw3 libglfw3-dev
```

**Fedora** or .rpm based repos:
```sh
su -
yum install libglfw libglfw-devel
```

#### Mac OSX
Mac users can use Brew:

```sh
brew install glfw3
```

#### Windows
Select the version of GLFW 3 that matches your system architecture (32-bit for i386, i586, and similar; 64-bit for x86-64 and amd64 systems).  Please note that the `glfw3.dll` file must match whichever environment was used to compile your Ruby installation.  It should then be placed into your "C:/Windows/System32" folder.

## OpenAL Support

The engine ships with its own OpenAL wrapper, but you should ensure you have a compatible OpenAL for your operating system.  

It is recommended to install **OpenAL Soft** instead, which provides updated functionality.  Linux and Mac system should be able to use these without changes; Windows systems will first check "C:/Windows/System32" and then the local *bin* folder before failing over to the implementation provided by Microsoft.

When the openal library is first loaded, the default device will be determined and established via `ALC` calls.  The device is automatically closed when the runtime exits.

## Additional credits

The following software has served as a model for the FFI libraries and Ogg functionality of the engine:

* [anibali / ruby-ogg](https://github.com/anibali/ruby-ogg) - as the basis for the Ogg library.
* [vaiorabbit / ruby-opengl](https://github.com/vaiorabbit/ruby-opengl) - as the basis for the OpenGL, OpenAL, and GLFW FFI libraries.
* [dmendel / bindata](https://github.com/dmendel/bindata) - for being crufty enough to make me write the `DataStruct` extension (and the `Bin_IO` support extension as well).
