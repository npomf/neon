require 'optparse'

# Handle command-line options
opts = []
OptionParser.new {|op|
    # Help
    op.on('-h', '--help'){
        puts op
    }

    # New libpath entry
    op.on('--lib PATH'){|path|
        $: << path unless $:include? path
    }

    # Require a gem before loading
    op.on('--require GEM'){|gem_name|
        require gem_name
    }

    # New import directory
    op.on('--include DIR_GLOB'){|dir|
        Dir.glob( dir ).each {|lib| require_relative lib unless File.directory? lib }
    }

    # Use extension packages -- NOT USED
    op.on('-x', '--use-extensions'){
        opts[:extensions] = true
    }

    # Open a console session after loading
    op.on('-i', '--interactive', 'Start an interactive console session'){
        opts[:interactive] = true
    }

    # Use the console in sandbox mode (default: false)
    op.on('-S', '--sandboxed', 'Start the console in sandboxed mode (default: false)'){
        opts[:sandboxed] = true
    }
}

# Expand path
libpath = File.expand_path('lib/')
$: << libpath unless $:.include? libpath

# Import everything
Dir.glob('lib/*.rb').each {|lib| require "./#{lib}" }

# Load extension packages -- should defer to configuration file to determine which are enabled
require_relative 'pkg/load' if opts[:extensions]

# Optional console session hook
Neon.add_thread( Console.new( opts[:sandboxed] || false ) )
