require_relative 'bounds'
require_relative '../util/persistent'
require_relative '../object/program'
require_relative '../opengl/transforms'
require_relative '../ext/vectors'

# Basic scene-graph element.
class Element
    include Persistent

    # Node count, used for ID generation
    @@nodes = 0

    # Identifier
    attr :id
    # Position vector
    attr_accessor :position
    # Rotation vector
    attr_accessor :rotation
    # Bounds
    attr_accessor :bounds
    # Visibility flag
    attr_writer :visible

    # Initialize.
    def initialize( id = nil, position: Vector[ 0.0, 0.0, 0.0, 1.0 ], rotation: Vector[ 0.0, 0.0, 0.0, 0.0 ],
                    visible: true, bounds: nil )
        @id = id || (@@nodes += 1)
        @position = position
        @rotation = rotation
        @bounds = nil
        @visible = visible
    end

    # Automated rendering.  Calls the various portions of the rendering sequence.
    def render()
        transform
        draw
        Transforms.pop
    end

    # Implementers should override this method to provide drawing operations.
    def draw()
    end

    # Check if this element is visible.
    def visible?()
        @visible
    end

    # Set up the transformation stack and current view matrix.
    def transform()
        Transforms.concat( @position, @rotation )
        prgm = Program.active
        prgm[:view_matrix] = Transforms.current
    end
    protected :transform

    # Dump the Node to a hash representation.
    def dump()
        super.merge({ id: @id, position: @position, rotation: @rotation })
    end
    protected :dump

    # Restore from MessagePack format.
    def self.msgpack_create( obj )
        id = obj.delete('id')
        obj.delete( MessagePack.create_id.to_s )
        new( id, obj.parameterize )
    end

    alias :location :position
    alias :location= :position=
end
