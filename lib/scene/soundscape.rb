require_relative '../util/persistent'
include EFX if require_relative '../openal/efx'

# Reverb properties for a sound-scape, describing the relative properties of a region or zone.
class Soundscape
    include Persistent

    # Single effect slot for handling reverb
    @@effect = nil

    # Reverb density
    attr_accessor :density
    # Reverb diffusion
    attr_accessor :diffusion
    # Reverb gain
    attr_accessor :gain
    # High frequency reverb gain
    attr_accessor :hf_gain
    # Reverb decay time
    attr_accessor :decay_time
    # High frequency decay ratio
    attr_accessor :hf_decay
    # Gain of reverberated reflections
    attr_accessor :reflection_gain
    # Delay in reverberated reflections
    attr_accessor :reflection_delay
    # Late reverb gain
    attr_accessor :late_gain
    # Late reverb delay
    attr_accessor :late_delay
    # Gain of high frequency sounds related to air absorption
    attr_accessor :hf_absorption_gain
    # Rolloff factor for the virtual "room"
    attr_accessor :rolloff
    # High frequency reverb decay limit
    attr_accessor :hf_decay_limit

    # Initialize.
    def initialize( density: 1.0, diffusion: 1.0, gain: 0.3162, decay_time: 1.49, reflection_gain: 0.05, 
                    reflection_delay: 0.007, late_gain: 1.2589, late_delay: 0.011, rolloff: 0.0, hf_gain: 0.8913, 
                    hf_decay: 0.83, hf_absorption_gain: 0.9943, hf_decay_limit: 1 )
        @density = density
        @diffusion = diffusion
        @gain = gain
        @decay_time = decay_time
        @reflection_gain = reflection_gain
        @reflection_delay = reflection_delay
        @late_gain = late_gain
        @late_delay = late_delay
        @rolloff = rolloff
        @hf_gain = hf_gain
        @hf_decay = hf_decay
        @hf_absorption_gain = hf_absorption_gain
        @hf_decay_limit = hf_decay_limit
    end

    # Commit all changes.  This **must** be called the first time the soundscape is invoked.
    def update()
        effect_slot if @@effect.nil?
        
        alEffecti( @@effect, AL_EFFECT_TYPE, AL_EFFECT_REVERB )
        alEffectf( @@effect, AL_REVERB_DENSITY, @density )
        alEffectf( @@effect, AL_REVERB_DIFFUSION, @diffusion )
        alEffectf( @@effect, AL_REVERB_GAIN, @gain )
        alEffectf( @@effect, AL_REVERB_GAINHF, @hf_gain )
        alEffectf( @@effect, AL_REVERB_DECAY_TIME, @decay_time )
        alEffectf( @@effect, AL_REVERB_DECAY_HFRATIO, @hf_decay )
        alEffectf( @@effect, AL_REVERB_REFLECTIONS_GAIN, @reflection_gain )
        alEffectf( @@effect, AL_REVERB_REFLECTIONS_DELAY, @reflection_delay )
        alEffectf( @@effect, AL_REVERB_LATE_REVERB_GAIN, @late_gain )
        alEffectf( @@effect, AL_REVERB_LATE_REVERB_DELAY, @late_delay )
        alEffectf( @@effect, AL_REVERB_AI_ABSORPTION_GAINHF, @hf_absorption_gain )
        alEffectf( @@effect, AL_REVERB_ROOM_ROLLOFF_FACTOR, @rolloff )
        alEffecti( @@effect, AL_REVERB_DECAY_HFLIMIT, @hf_decay_limit )
    end
    alias :commit :update

    # Generates the corresponding effect slot for the soundscape.  If one already exists, it is returned.
    def effect_slot()
        if @@effect.nil?
            efxptr = 0.pointer
            alGenEffects( 1, efxptr )
            @@effect = efxptr.read( :uint )
        end
        @@effect
    end

    # Convert to hash format.
    def dump()
        super.merge({
            density: @density,
            diffusion: @diffusion,
            gain: @gain,
            hf_gain: @hf_gain,
            decay_time: @decay_time,
            hf_decay: @hf_decay,
            reflection_gain: @reflection_gain,
            reflection_delay: @reflection_delay,
            late_gain: @late_gain,
            late_delay: @late_delay,
            hf_absoprtion_gain: @hf_asbsorption_gain,
            rolloff: @rolloff,
            hf_decay_limit: @hf_decay_limit
        })
    end
    protected :dump

    # Create a soundscape from an array of arguments.
    def self.from( density, diffusion, gain, hf_gain, decay_time, hf_decay, reflection_gain, reflection_delay, late_gain, 
                   late_delay, hf_absorption_gain, rolloff,  hf_decay_limit )
        new( 
            density: density, diffusion: diffusion, gain: gain, decay_time: decay_time, reflection_gain: reflection_gain,
            late_gain: late_gain, late_delay: late_delay, rolloff: rolloff, hf_gain: hf_gain, hf_decay: hf_decay,
            hf_absorption_gain: hf_absorption_gain, hf_decay_limit: hf_decay_limit
        )
    end

    # Default preset
    DEFAULT = Soundscape.new()
end
