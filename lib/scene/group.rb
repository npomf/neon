require_relative 'element'
require_relative '../opengl/transforms'

# A group is any collection of nodes.
class Group < Element

    # Nodes within the Group
    attr_reader :nodes

    # Initialize.
    def initialize( id = nil, nodes = [], position: Vector[ 0.0, 0.0, 0.0, 1.0 ], rotation: Vector[ 0.0, 0.0, 0.0, 0.0 ],
                    bounds: nil )
        super( id, bounds: bounds, position: position, rotation: rotation )
        @nodes = nodes
    end

    # Draw this group.
    def draw()
        @nodes.each {|node| node.render() }
    end

    # Append a node to the group.
    def add( node )
        @nodes << node if node.is_a? Element
    end
    alias :<< :add

    # Insert a node into the group at the specified position.
    def insert( position, node )
        @nodes.insert( position, node ) if node.is_a? Element
    end

    # Find a node by its ID.
    def find( id )
        @nodes.each {|node| return node if node.id.eql? id } if id
    end

    # Remove a node from the group.  `node` may be an index or Node.
    def remove( node )
        (node.is_a? Fixnum) ? @nodes.delete_at( node ) : @nodes.delete( node )
    end

    # Flatten this Group into an array of all child elements.  This is called recursively.
    def flatten()
        nodes.map {|node| (node.is_a? Group) ? node.flatten : node }
    end

    # Dump the Group to a hash representation.
    def dump()
        super.merge({ nodes: @nodes })
    end
    protected :dump

    # Restore from MessagePack format.
    def self.msgpack_create( obj )
        id, nodes = obj.delete('id'), obj.delete('nodes')
        new( id, nodes, obj.parameterize )
    end
end
