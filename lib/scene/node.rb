require 'thread_safe'
require_relative 'element'

# A node is any element in the scene which uses one or more resources, and can have specialized information attached to
# it.  This includes collision, bounding data, and transformation data.
#
# Nodes may have Property objects added to them.
class Node < Element
    include Properties
    
    # Texture maps; supports multiple elements
    attr_reader :texture
    # Vertex Buffer Object
    attr_reader :vbo
    # Vertex Array object
    attr_reader :vao
    # Shader Program
    attr_reader :program

    # Initialize.
    def initialize( id = nil, position: Vector[ 0.0, 0.0, 0.0, 1.0 ], rotation: Vector[ 0.0, 0.0, 0.0, 0.0 ],
                    bounds: nil, texture: [], vbo: nil, vao: nil, program: nil, **properties )
        super( id, bounds: bounds, position: position, rotation: rotation )
        @texture = texture
        @vbo = vbo
        @vao = vao
        @program = program
        @property = ThreadSafe::Hash.new().merge( properties )
    end

    # Draw this node.
    def draw()
        @program.swap unless @program.nil?
        @texture.each_with_index {|texture, unit| texture.bind( GL.const_get("GL_TEXTURE#{unit}") ) }
        @vbo.bind unless @vbo.nil?
        @vao.draw unless @vao.nil?
        @vbo.unbind unless @vao.nil?
        @texture.each_with_index {|texture, unit| texture.unbind( GL.const_get("GL_TEXTURE#{unit}") ) }
        Program.restore unless @program.nil?
    end

    # Dump the Node to a hash representation.
    def dump()
        super.merge( texture: @texture, vbo: @vbo, vao: @vao ).merge( @property.to_h )
    end
    protected :dump
    
    alias :vertex_buffer :vbo
    alias :vertices :vbo
    alias :vertex_array :vao
end
