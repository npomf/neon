require_relative 'bounds'

# Contains a series of vertices that describe the planes (and thus the mesh) used for collisions on a Resource.  Unlike
# a normal Bounds, CollisionMesh objects do not describe a `to`.  They may optional use their `start` as a relative
# offset for the vertices.
class CollisionMesh < Bounds

    # Triangle strip of vertices
    attr_reader :vertices

    # TODO
end
