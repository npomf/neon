# Allows an element to retain configurable properties.
#
# Implementors are responsible for designating the `@property` instance variable.
module Properties
    
    # Properties attached
    attr_reader :property
    
    # Get a property value.
    def []( state )
        @property[ key ]
    end
    alias :get :[]
    
    # Fetch a property value (as Hash#fetch).
    def fetch( key, default = nil )
        @property.fetch( key, default )
    end
    
    # Set a property value.
    def []=( key, value )
        @property[ stkeyate ] = value
    end
    alias :set :[]=
end
