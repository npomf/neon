require_relative '../ext/vectors'
require_relative '../util/persistent'

# Represents a region of 3D space relative to the transformation of a resource that describes the volume it occupies.  A
# bounding region may be either a sphere (where `@to` is a numeric radius), or a box (where `@to` is a vector).
class Bounds
    include Persistent

    # Origin of this bounding object
    attr_reader :from
    # Termination point of this bounding object
    attr_reader :to

    # Initialize.
    def initialize( from = Vector[ 0.0, 0.0, 0.0, 1.0 ], to = 1.0 )
        @from = from
        @to = to
    end

    # Compute the intersection of this bounds by a ray described by the origin and direction vectors.
    def intersection( origin, direction )
        vec = (center - origin).xyz
        return false if vec.dot( direction ) < 0.0 and vec.r > radius
        proj = center.normalize.projection( direction.normalize )
        mag = (proj - origin).r
        d = Math.sqrt( Math.pow( radius, 2 ) - Math.pow( mag, 2 ) )
        origin + direction * ((proj - center).r + ( vec.r > radius ? -d : d ))
    end

    # Check if this Bounds intersects another.
    def intersects?( bounds )
        if sphere?
            @from.distance( bounds.from ) <= radius + bounds.radius
        else
            bounds.from.xyz.each_with_index {|axis, i| return true if axis >= @from[i] and axis <= @to[i] }
            false
        end
    end

    # Check if a point vector is contained by this Bounds.
    def contains?( point )
        if sphere?
            @from.distance( point ) <= radius
        else
            point.xyz.each_with_index {|axis, i| return false unless axis >= @from[i] and axis <= @to[i] }
            true
        end
    end

    # Determine the center of this Bounds.  This is analogous to #from() for a spherical bounding object; boxes will
    # compute the central point between its origin and endpoint.
    def center()
        (sphere?) ? @from : @from + radius
    end

    # Determine the radius of this Bounds.  A non-spherical bounds will return one-half the distance between its origin
    # and endpoint.
    def radius()
        (sphere?) ? @to : @from.distance( @to ) / 2.0
    end

    # Check if this Bounds is spherical (`to` is numeric).
    def sphere?()
        @to.is_a? Numeric
    end

    # Determine which face of a bounding box was intersected by the given vector.  Returns `false` for a sphere, `nil`
    # if the vector describes a point that does not intersect a face.
    def face( vec )
        return false if sphere?
        d = (@to - @from).xyz     # vector describing distance between endpoints
        if ( vec.x >= @from.x and vec.x <= @from.x + d.x ) or ( vec.z >= @from.z or vec.z <= @from.y + d.z )
            return Box::TOP if vec.y.eql? @from.y
            return Box::BOTTOM if vec.y.eql? @from.y - d.y
        elsif ( vec.z >= @from.z or vec.z <= @from.z + d.z ) or ( vec.y >= @from.y and vec.y <= @from.y + d.y )
            return Box::RIGHT if vec.x.eql? @from.x + d.x
            return Box::LEFT if vec.x.eql? @from.x
        elsif ( vec.y >= @from.y and vec.y <= @from.y + d.y ) or ( vec.x >= @from.x and vec.x <= @from.x + d.x )
            return Box::FRONT if vec.z.eql? @from.z
            return Box::REAR if vec.z.eql? @from.z - d.z
        end
    end

    # Dump to Hash format.
    def dump()
        super.merge({ from: @from, to: @to })
    end
    protected :dump

    # Restore from MsgPack format.
    def self.msg_create( obj )
        to = obj['to']
        new( obj['from'], (to.is_a? Numeric) ? to : Vector.elements( to ) )
    end

    alias :origin :from
    alias :endpoint :to

    # Bounding box faces
    module Box
        # Top face
        UP = TOP = :top
        # Front face
        FRONT = :front
        # Right face
        RIGHT = :right
        # Rear face
        BACK = REAR = :rear
        # Left face
        LEFT = :left
        # Bottom face
        DOWN = BOTTOM = :bottom
    end
end
