require_relative '../ext/vectors'
require_relative 'group'

# Represents a slice of the virtual world, and provides information on ambient lighting, sound-scape, and other world
# features within the region of space it represents.
class Region < Group

    # Specular lighting color as RGB vector
    attr_accessor :specularity
    # Ambient lighting color as RGB vector
    attr_accessor :ambient
    # Diffuse lighting color as RGB vector
    attr_accessor :diffuse
    # Lighting direction
    attr_accessor :light_direction
    # Current Soundscape object
    attr_accessor :soundscape

    # Initialize.
    def initialize( id = nil, nodes = [], specularity: nil, ambient: nil, diffuse: nil, soundscape: Soundscape::DEFAULT,
                    light_direction: Vector[ 0.0, -1.0, 0.0, 0.0 ] )
        super( id, nodes )
        @specularity = specularity
        @ambient = ambient
        @diffuse = diffuse
        @light_direction = light_direction
        @soundscape = soundscape
    end

    # Configure the region for drawing, the draw its child nodes.
    def draw()
        prgm = Program.active
        prgm[:ambient_lighting] = @ambient
        prgm[:diffuse_lighting] = @diffuse
        prgm[:specular_lighting] = @specularity
        prgm[:lighting_direction] = @light_direction
        @soundscape.update
        super()
    end

    # Dump to Hash format.
    def dump()
        super.merge({ 
            lighting: @lighting, 
            specularity: @specularity, 
            ambient: @ambient, 
            diffuse: @diffuse, 
            light_direction: @light_direction, 
            soundscape: @soundscape 
        })
    end
    protected :dump

    # Restore from MessagePack format.
    def self.msgpack_create( obj )
        id, nodes = obj.delete('id'), obj.delete('nodes')
        obj['soundscape'] = Soundscape.const_get( obj['soundscape'] )
        new( id, nodes, obj.parameterize )
    end
end
