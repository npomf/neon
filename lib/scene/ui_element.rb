require_relative 'element'
require_relative 'properties'
require_relative '../object/script'

# Allows a node to be "picked" (selected) by a mouse click and perform a scripted action.  This can be useful for the
# creation of buttons, check boxes, toggles, and inputs.
class UiElement < Node
    include Properties
    
    # Pick/Select event
    attr_accessor :on_select
    
    # Initialize.
    #
    # The `hotspot` is the bounds associated with the element and is used to determine whether it has been selected.
    def initialize( id = nil, bounds:, position: Vector[ 0.0, 0.0, 0.0, 1.0 ], rotation: Vector[ 0.0, 0.0, 0.0, 0.0 ], 
                    **properties, &evt )
        super( id, bounds: bounds, position: position, rotation: rotation, properties )
        @on_select = evt
    end
    
    # Fire the pick/select event.
    def pick( *args )
        @on_select.call( *args )
    end
    alias :select :pick
    
    # Dump the Node to a hash representation.
    def dump()
        super  # TODO script
    end
    protected :dump
    
    # Restore from MessagePack format.
    def self.msgpack_create( obj )
        node = super
        node.on_select = Script.new("UI-script-#{obj['id']}", obj['script'] ) if obj.key? 'script'
        node
    end

    alias :hotspot :bounds
    alias :hotspot= :bounds=
end
