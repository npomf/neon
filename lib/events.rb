require 'thread_safe'
require_relative 'core'
require_relative 'event/event'

# Event receivers
$receivers = ThreadSafe::Array.new
# Event queue
$evtqueue = ThreadSafe::Array.new
# The event dispatcher thread
$dispatcher = Thread.new {
    loop {
        sleep 0.01 while $evtqueue.empty?
        evt = $evtqueue.shift   # FiFo
        next if evt.expired?    # discard expired events, if a ttl was set
        $receivers.each {|r| r.notify( evt.mask, *evt.args, **evt.kwargs ) rescue nil }
    }
}

# Add dispatcher to the thread pool
Neon << $dispatcher

Kernel.module_eval %q{

    # Local hooks
    attr :evthooks

    # Register a new event hook for this object.
    def on( mask, &block )
        @evthooks = ThreadSafe::Cache.new() if @evthooks.nil?
        @evthooks[ mask ] = block
    end

    # Unregister an event hook.
    def off( mask )
        @evthooks.delete( mask ) if not @evthooks.nil? and @evthooks.key? mask
    end

    # Notify this object of an event.
    def notify( mask, *args, **kwargs )
        @evthooks[ mask ].call( *args, **kwargs ) if not @evthooks.nil? and @evthooks.key? mask
    end

    # Emit an event to all registered receivers, adding the event to the dispatching thread.
    def emit( mask, *args, **kwargs )
        submit( mask, *args, **kwargs )
    end
}

# Register a new event receiver.
def register( who )
    $receivers << who unless $receivers.include? who
end

# Unregister an event receiver.
def unregister( who )
    $receivers.delete( who )
end

# Submit an event.  May submit a raw mask and parameters or a bundled Event.  Returns the Event object.
def submit( what, ttl: nil, *args, **kwargs )
    what = Event.new( what, ttl: ttl, *args, **kwargs ) unless what.is_a? Event
    $evtqueue << what
    what
end
