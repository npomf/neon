include AL if require_relative 'openal/al'
include ALC if require_relative 'openal/alc'
include EFX if require_relative 'openal/efx'

# Setup
AL.load_lib
ALC.load_lib
EFX.load_lib

module Neon
    extend self
    
    # OpenAL device
    attr_reader :al_device
    # OpenAL context
    attr_reader :al_context

    # Set up device and context
    @al_device = alcOpenDevice( [0].pointer( :char ) )
    @al_context = alcCreateContext( @al_device, [0].pointer( :int ) )
    alcMakeContextCurrent( @al_context )
end

# Get packages
require_relative 'openal/listener'
require_relative 'openal/sound'

# Finalizer for safe cleanup
at_exit {
    alcDestroyContext( Neon.al_context ) unless Neon.al_context.nil?
    alcCloseDevice( Neon.al_device ) unless Neon.al_device.nil?
}
