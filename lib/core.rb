require 'thread_safe'
require 'json'
require_relative 'events'
require_relative 'ext/threading'

# Root of the Neon engine.  Threads may be spawned directly from the engine and will be tracked internally using its
# thread pool.
module Neon
    include ThreadPoolMixin
    extend self

    # System properties
    attr_accessor :properties
    @properties = ThreadSafe::Hash.new

    # Evaluation bindings
    attr_reader :evaluator
    @evaluator = binding()

    # Process Ruby code within the evaluator sandbox.  This allows state changes to be retained during runtime.
    def eval( string )
        @evaluator.eval( string )
    end
    alias :sandbox :eval

    # Import a JSON property file.
    def configure( file )
        put( JSON.parse( open( file ).read ) )
    end
    alias :import :configure

    # Export configuration properties to a JSON file.  If `pretty` is falsey, the exported JSON will *not* be pretty-
    # printed for legibility.
    def save_configuration( file, pretty = true )
        open( file, 'w'){|io| io.write( pretty ? JSON.pretty_generate( @properties ) : @properties.to_json ) }
    end
    alias :export :save_configuration

    # Set multiple properties.
    def put( **kwargs )
        @properties.merge! kwargs
    end

    # Set a property.
    def set( key, value )
        @properties[ key ] = value
    end
    alias :[]= :set

    # Safely get a property.  Shorthand for using `.properties.fetch( key, default )`.
    def get( key, default = nil )
        (@properties.nil?) ? default : @properties.fetch( key, default )
    end
    alias :[] :get

    # Safe termination.
    def terminate( status = true )
        emit( :exit )
        # TODO
        kill    # kills all managed threads
        exit( status )
    end
end

# Shorthand form
Ne = Neon
