require_relative '../ext/auto_msgpack'

# Simple serialization mixin.
module Persistent

    # Dump to Hash format.
    def dump()
        { MessagePack.create_id => self.class.name }
    end
    protected :dump

    # Convert to MsgPack format.
    def to_msgpack()
        dump.to_msgpack()
    end

    # Anonymouse creation method.
    def self.msg_create( obj )
        new( obj.parameterize )
    end
end
