include GL if require_relative 'opengl/gl_core'
include GLFW if require_relative 'opengl/glfw3'

# Setup
GL.load_lib
GLFW.load_lib

# Initialize GLFW 3
glfwInit()

# Get packages
require_relative 'opengl/window'
require_relative 'opengl/renderer'
require_relative 'opengl/camera'

# Finalizer for safe cleanup
at_exit {
    Window.instance.destroy() unless Window.instance.nil?
    glfwTerminate() 
}
