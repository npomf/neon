# Event object container.
class Event

    # Event mask
    attr_reader :mask
    # Creation time
    attr_reader :timestamp
    # Time To Live (ttl), in seconds
    attr_reader :ttl
    # Arguments
    attr_reader :args
    # Keywords
    attr_reader :kwargs

    # Initialize.
    def initialize( mask, ttl: nil, *args, **kwargs )
        @mask = mask
        @timestamp = Time.now
        @ttl = ttl
        @args = args
        @kwargs = kwargs
    end

    # Check if an event has expired and should *not* be processed.
    def expired?()
        @ttl ? @timestamp + @ttl < Time.now : false
    end
end
