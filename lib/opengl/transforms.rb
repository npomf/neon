require_relative '../ext/vectors'

# Tracks all transforms as a stack.
module Transform
    extend self

    # Transform stack
    @@stack = []
    
    # Get the current transform matrix.
    def current()
        @@stack.last
    end
    alias :this :current

    # Push a new transform.
    def push( pos, rot )
        @@stack.push( view_matrix( pos, rot ) )
    end

    # Push a new transform, concatenating it from the previous one.
    def concat( pos, rot )
        m = view_matrix( pos, rot )
        m += @@stack.last unless @@stack.empty?
        @@stack.push( m )
    end

    # Pop a transform, returning it.
    def pop()
        @@stack.pop
    end

    # Build a view matrix.
    def view_matrix( pos, rot )
        rx, ry, rz = rot.xyz.map {|axis| Math.radian( axis ) }
        translation = Matrix[
            [ 1.0, 0.0, 0.0, -pos.x ],
            [ 0.0, 1.0, 0.0, -pos.y ],
            [ 0.0, 0.0, 1.0, -pos.z ],
            [ 0.0, 0.0, 0.0, 1.0 ]
        ]
        orientation = Matrix[
            [ Math.cos( ry ) * Math.cos( rz ), -Math.sin( rz ), Math.sin( ry ), 0.0 ],
            [ Math.sin( rz ), Math.cos( rx ) * Math.cos( rz ), -Math.sin( rx ), 0.0 ],
            [ -Math.sin( ry ), Math.sin( rx ), Math.cos( rx ) * Math.cos( ry ), 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0 ]
        ]
        translation * orientation
    end
end
