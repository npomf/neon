require_relative '../object/reference'
require_relative '../ext/arithmetic'
require_relative '../ext/vectors'

# Camera and frustum dimension data.
class Camera
    include Reference

    # Camera ID
    attr_accessor :id
    # Position vector
    attr_reader :position
    # Rotation vector
    attr_reader :rotation
    # Viewport dimensions, in pixels
    attr_reader :viewport
    # View matrix
    attr_reader :view_matrix
    # Field of view angle, in degrees
    attr_accessor :field_of_view
    # Near image plate)
    attr_reader :near
    # Far image plate
    attr_reader :far
    # Projection matrix
    attr_reader :projection_matrix
    # Minimum angle of attenuation, in degrees
    attr_accessor :attenuation

    # Initialize.
    def initialize( position: Vector[0.0, 0.0, 0.0, 1.0], rotation: Vector[0.0, 0.0, -1.0, 0.0], field_of_view: 90.0,
                    near: 1.0, far: 1e4, attenuation: 1e-2, name: 'Default-Camera' )
        @position = position
        @rotation = rotation
        @field_of_view = field_of_view
        @near = near
        @far = far
        @attenuation = attenuation
        @viewport = Viewport.new( 0.0, 0.0 )

        # Compute matrices and register the instance
        build_view_matrix
        build_projection_matrix
        register
    end

    # Test whether a point (and optional bounding radius) are visible within the frustum via a radar test.  If instead
    # a Node is passed, it will be coerced into the appropriate values (and its radius extracted).
    def visible?( point, radius = 0.0 )
        if point.is_a? Node
            radius = point.bounds.radius if point.bounds
            point = point.position
        end

        # Radar test for Z-axis
        v = (point - @view_matrix).normalize.xyz
        z = v.dot( point.z )
        return false if z > @far + radius or z < @near - radius

        # Radar test for Y-axis
        fov = Math.radian( @field_of_view )
        t = Math.tan( fov )
        d = radius > 0.0 ? radius * Math.pow( Math.cos( fov ), -1 ) : 0.0
        y = v.dot( point.y )
        z *= t
        return false if y > z + d or y < -z - d

        # Radar test for X-axis
        d = radius > 0.0 ? * Math.pow( Math.cos( Math.atan( t ) ), -1 ) : 0.0
        x = v.dot( point.x )
        z *= aspect_ratio
        not x > z + d or x < -z -d
    end
    alias :on_screen? :visible?

    # Perform picking using the on-screen coordinates and the list of provided nodes.
    def pick( x, y, nodes )
        vx, vy = @viewport
        clip = Vector[ (2.0 * x) / vx - 1.0, 1.0 - (2.0 * y) / vy, -1.0, 1.0 ]
        projection = @projection_matrix.inverse * clip
        eye = Vector[ projection.x, projection.y, -1.0, 0.0 ]
        world = (@view_matrix.inverse * eye).xyz.normalize

        # Perform ray picking against bunding spheres.
        nodes.select {|node| visible? node if node.visible? and node.bounds }
             .sort {|a, b| b.position.z <=> a.position.z }
             .select {|node| node.bounds.intersection( @position, world ) }
    end

    # Test whether a point (and optional bounding radius) are attenuated too small to be considered visible.
    def attenuated?( point, radius = 0.1 )
        radius / Math.sqrt( (point - @position).xyz.map {|axis| Math.pow( axis, 2 ) }.inject( :+ ) ) < @attenuation
    end

    # Compute the depth of field.
    def depth_of_field()
        @far - @near
    end
    alias :depth :depth_of_field

    # Compute the aspect ratio of the viewport.
    def aspect_ratio()
        @viewport.width.to_f / @viewport.height.to_f
    end
    alias :aspect :aspect_ratio

    # Compute the visible arc length of the frustum.
    def arc_length()
        Math.tan( Math.radian( @field_of_view ) * 0.5 * @near )
    end
    alias :arc :arc_length

    # Move the camera; this is equivalent to calling `cam.position += vec3`.
    def move( vec )
        position = @position + vec
    end

    # Set the position vector.
    def position=( vec )
        @position = vec
        build_view_matrix
    end

    # Rotate the camera; this is equivalent to calling `cam.rotation += vec3`.
    def rotate( vec )
        rotation = @rotation + vec
    end

    # Set the rotation vector.
    def rotation=( vec )
        @rotation = vec
        build_view_matrix
    end

    # Set the camera transformation.
    def transformation=( pos, rot )
        @position = pos
        @rotation = rot
        build_view_matrix
    end

    # Set the near image plate depth.
    def near=( u )
        @near = u
        build_projection_matrix
    end

    # Set the far image plate depth.
    def far=( u )
        @far = u
        build_projection_matrix
    end

    # Set the frustum.
    def frustum=( near, far )
        @near = near
        @far = far
        build_projection_matrix
    end

    # Set the viewport dimensions.
    def viewport=( width, height )
        @viewport.width = width
        @viewport.height = height
    end

    # Generate the view matrix for the camera.
    def build_view_matrix()
        rx, ry, rz = @rotation.xyz.map {|axis| Math.radian( axis ) }
        translation = Matrix[
            [ 1.0, 0.0, 0.0, -@position.x ],
            [ 0.0, 1.0, 0.0, -@position.y ],
            [ 0.0, 0.0, 1.0, -@position.z ],
            [ 0.0, 0.0, 0.0, 1.0 ]
        ]
        orientation = Matrix[
            [ Math.cos( ry ) * Math.cos( rz ), -Math.sin( rz ), Math.sin( ry ), 0.0 ],
            [ Math.sin( rz ), Math.cos( rx ) * Math.cos( rz ), -Math.sin( rx ), 0.0 ],
            [ -Math.sin( ry ), Math.sin( rx ), Math.cos( rx ) * Math.cos( ry ), 0.0 ],
            [ 0.0, 0.0, 0.0, 1.0 ]
        ]
        @view_matrix = translation * orientation
    end
    private :build_view_matrix

    # Generate the projection matrix for the camera.
    def build_projection_matrix()
        @projection_matrix = Matrix[
            [ (2.0 * @near) / Math.pow( arc_length * aspect_ratio, 2 ), 0.0, 0.0, 0.0 ],
            [ 0.0, @near / arc_length, 0.0, 0.0 ],
            [ 0.0, 0.0, -(@far + @near) / depth_of_field.to_f, (-2.0 * @near * @far) / depth_of_field.to_f ],
            [ 0.0, 0.0, -1.0, 0.0 ]
        ]
    end
    private :build_projection_matrix

    alias :location :position
    alias :location= :position=
    alias :fov :field_of_view
    alias :fov= :field_of_view=

    # Simple container class for describing the viewport dimensions.
    class Viewport
        # Viewport width, in pixels
        attr_accessor :width
        # Viewport height, in pixels
        attr_accessor :height
        # X-axis of viewport origin; screen relative
        attr_accessor :x
        # Y-axis of viewport origin; screen relative
        attr_accessor :y

        # Initialize.
        def initialize( x = nil, y = nil, width, height )
            @x = x
            @y = y
            @width = width
            @height = height
        end
    end
end
