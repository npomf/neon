require 'fiddle'
require 'fiddle/import'

# GLFW 3 FFI wrapper for Ruby.
module GLFW
    extend Fiddle::Importer

    # Map of OpenGL function pointers
    GLFW_FUNCTIONS_MAP = {}
    # GLFWvidmode struct
    GLFWvidmode = struct([
        'int width',
        'int height',
        'int redBits',
        'int greenBits',
        'int blueBits',
        'int refreshRate'
    ])
    # GLFWgammaramp struct
    GLFWgammaramp = struct([
        'unsigned short* red',
        'unsigned short* green',
        'unsigned short* blue',
        'unsigned int size'
    ])

    # Signatures for callback function external C functions; used by create_callback()
    @@callback_extern = {
        GLFWglproc: 'void GLFWglProc()',
        GLFWerrorfun: 'void GLFWerrorFun( int, const char* )',
        GLFWwindowposfun: 'void GLFWwindowposfun( void* int, int )',
        GLFWwindowsizefun: 'void GLFWwindowsizefun( void* int, int )',
        GLFWwindowclosefun: 'void GLFWwindowclosefun( void* )',
        GLFWwindowrefreshfun: 'void GLFWwindowrefreshfun( void* )',
        GLFWwindowfocusfun: 'void GLFWwindowfocusfun( void*, int )',
        GLFWwindowiconifyfun: 'void GLFWwindowiconifyfun( void*, int )',
        GLFWframebuffersizefun: 'void GLFWframebuffersizefun( void*, int, int )',
        GLFWmousebuttonfun: 'void GLFWmousebuttonfun( void*, int, int, int )',
        GLFWcursorposfun: 'void GLFWcursorposfun( void*, double, double )',
        GLFWcursorenterfun: 'void GLFWcursorenterfun( void*, int )',
        GLFWscrollfun: 'void GLFWscrollfun( void*, double, double )',
        GLFWkeyfun: 'void GLFWkeyfun( void*, int, int, int, int )',
        GLFWcharfun: 'void GLFWcharfun( void*, unsigned int )',
        GLFWmonitorfun: 'void GLFWmonitorfun( void*, int )'
    }

    GLFW_VERSION_MAJOR          = 3
    GLFW_VERSION_MINOR          = 0
    GLFW_VERSION_REVISION       = 1

    GLFW_RELEASE                = 0
    GLFW_PRESS                  = 1
    GLFW_REPEAT                 = 2

    GLFW_KEY_UNKNOWN            = -1
    GLFW_KEY_SPACE              = 32
    GLFW_KEY_APOSTROPHE         = 39  #  '
    GLFW_KEY_COMMA              = 44  #  ,
    GLFW_KEY_MINUS              = 45  #  -
    GLFW_KEY_PERIOD             = 46  #  .
    GLFW_KEY_SLASH              = 47  #  /
    GLFW_KEY_0                  = 48
    GLFW_KEY_1                  = 49
    GLFW_KEY_2                  = 50
    GLFW_KEY_3                  = 51
    GLFW_KEY_4                  = 52
    GLFW_KEY_5                  = 53
    GLFW_KEY_6                  = 54
    GLFW_KEY_7                  = 55
    GLFW_KEY_8                  = 56
    GLFW_KEY_9                  = 57
    GLFW_KEY_SEMICOLON          = 59  #  ;
    GLFW_KEY_EQUAL              = 61  #  =
    GLFW_KEY_A                  = 65
    GLFW_KEY_B                  = 66
    GLFW_KEY_C                  = 67
    GLFW_KEY_D                  = 68
    GLFW_KEY_E                  = 69
    GLFW_KEY_F                  = 70
    GLFW_KEY_G                  = 71
    GLFW_KEY_H                  = 72
    GLFW_KEY_I                  = 73
    GLFW_KEY_J                  = 74
    GLFW_KEY_K                  = 75
    GLFW_KEY_L                  = 76
    GLFW_KEY_M                  = 77
    GLFW_KEY_N                  = 78
    GLFW_KEY_O                  = 79
    GLFW_KEY_P                  = 80
    GLFW_KEY_Q                  = 81
    GLFW_KEY_R                  = 82
    GLFW_KEY_S                  = 83
    GLFW_KEY_T                  = 84
    GLFW_KEY_U                  = 85
    GLFW_KEY_V                  = 86
    GLFW_KEY_W                  = 87
    GLFW_KEY_X                  = 88
    GLFW_KEY_Y                  = 89
    GLFW_KEY_Z                  = 90
    GLFW_KEY_LEFT_BRACKET       = 91  #  [
    GLFW_KEY_BACKSLASH          = 92  #  \
    GLFW_KEY_RIGHT_BRACKET      = 93  #  ]
    GLFW_KEY_GRAVE_ACCENT       = 96  #  `
    GLFW_KEY_WORLD_1            = 161 #  non-US #1
    GLFW_KEY_WORLD_2            = 162 #  non-US #2
    GLFW_KEY_ESCAPE             = 256
    GLFW_KEY_ENTER              = 257
    GLFW_KEY_TAB                = 258
    GLFW_KEY_BACKSPACE          = 259
    GLFW_KEY_INSERT             = 260
    GLFW_KEY_DELETE             = 261
    GLFW_KEY_RIGHT              = 262
    GLFW_KEY_LEFT               = 263
    GLFW_KEY_DOWN               = 264
    GLFW_KEY_UP                 = 265
    GLFW_KEY_PAGE_UP            = 266
    GLFW_KEY_PAGE_DOWN          = 267
    GLFW_KEY_HOME               = 268
    GLFW_KEY_END                = 269
    GLFW_KEY_CAPS_LOCK          = 280
    GLFW_KEY_SCROLL_LOCK        = 281
    GLFW_KEY_NUM_LOCK           = 282
    GLFW_KEY_PRINT_SCREEN       = 283
    GLFW_KEY_PAUSE              = 284
    GLFW_KEY_F1                 = 290
    GLFW_KEY_F2                 = 291
    GLFW_KEY_F3                 = 292
    GLFW_KEY_F4                 = 293
    GLFW_KEY_F5                 = 294
    GLFW_KEY_F6                 = 295
    GLFW_KEY_F7                 = 296
    GLFW_KEY_F8                 = 297
    GLFW_KEY_F9                 = 298
    GLFW_KEY_F10                = 299
    GLFW_KEY_F11                = 300
    GLFW_KEY_F12                = 301
    GLFW_KEY_F13                = 302
    GLFW_KEY_F14                = 303
    GLFW_KEY_F15                = 304
    GLFW_KEY_F16                = 305
    GLFW_KEY_F17                = 306
    GLFW_KEY_F18                = 307
    GLFW_KEY_F19                = 308
    GLFW_KEY_F20                = 309
    GLFW_KEY_F21                = 310
    GLFW_KEY_F22                = 311
    GLFW_KEY_F23                = 312
    GLFW_KEY_F24                = 313
    GLFW_KEY_F25                = 314
    GLFW_KEY_KP_0               = 320
    GLFW_KEY_KP_1               = 321
    GLFW_KEY_KP_2               = 322
    GLFW_KEY_KP_3               = 323
    GLFW_KEY_KP_4               = 324
    GLFW_KEY_KP_5               = 325
    GLFW_KEY_KP_6               = 326
    GLFW_KEY_KP_7               = 327
    GLFW_KEY_KP_8               = 328
    GLFW_KEY_KP_9               = 329
    GLFW_KEY_KP_DECIMAL         = 330
    GLFW_KEY_KP_DIVIDE          = 331
    GLFW_KEY_KP_MULTIPLY        = 332
    GLFW_KEY_KP_SUBTRACT        = 333
    GLFW_KEY_KP_ADD             = 334
    GLFW_KEY_KP_ENTER           = 335
    GLFW_KEY_KP_EQUAL           = 336
    GLFW_KEY_LEFT_SHIFT         = 340
    GLFW_KEY_LEFT_CONTROL       = 341
    GLFW_KEY_LEFT_ALT           = 342
    GLFW_KEY_LEFT_SUPER         = 343
    GLFW_KEY_RIGHT_SHIFT        = 344
    GLFW_KEY_RIGHT_CONTROL      = 345
    GLFW_KEY_RIGHT_ALT          = 346
    GLFW_KEY_RIGHT_SUPER        = 347
    GLFW_KEY_MENU               = 348
    GLFW_KEY_LAST               = GLFW_KEY_MENU

    GLFW_MOD_SHIFT              = 0x01
    GLFW_MOD_CONTROL            = 0x02
    GLFW_MOD_ALT                = 0x04
    GLFW_MOD_SUPER              = 0x08

    GLFW_MOUSE_BUTTON_1         = 0
    GLFW_MOUSE_BUTTON_2         = 1
    GLFW_MOUSE_BUTTON_3         = 2
    GLFW_MOUSE_BUTTON_4         = 3
    GLFW_MOUSE_BUTTON_5         = 4
    GLFW_MOUSE_BUTTON_6         = 5
    GLFW_MOUSE_BUTTON_7         = 6
    GLFW_MOUSE_BUTTON_8         = 7
    GLFW_MOUSE_BUTTON_LAST      = GLFW_MOUSE_BUTTON_8
    GLFW_MOUSE_BUTTON_LEFT      = GLFW_MOUSE_BUTTON_1
    GLFW_MOUSE_BUTTON_RIGHT     = GLFW_MOUSE_BUTTON_2
    GLFW_MOUSE_BUTTON_MIDDLE    = GLFW_MOUSE_BUTTON_3

    GLFW_JOYSTICK_1             = 0
    GLFW_JOYSTICK_2             = 1
    GLFW_JOYSTICK_3             = 2
    GLFW_JOYSTICK_4             = 3
    GLFW_JOYSTICK_5             = 4
    GLFW_JOYSTICK_6             = 5
    GLFW_JOYSTICK_7             = 6
    GLFW_JOYSTICK_8             = 7
    GLFW_JOYSTICK_9             = 8
    GLFW_JOYSTICK_10            = 9
    GLFW_JOYSTICK_11            = 10
    GLFW_JOYSTICK_12            = 11
    GLFW_JOYSTICK_13            = 12
    GLFW_JOYSTICK_14            = 13
    GLFW_JOYSTICK_15            = 14
    GLFW_JOYSTICK_16            = 15
    GLFW_JOYSTICK_LAST          = GLFW_JOYSTICK_16

    GLFW_NOT_INITIALIZED        = 0x10001
    GLFW_NO_CURRENT_CONTEXT     = 0x10002
    GLFW_INVALID_ENUM           = 0x10003
    GLFW_INVALID_VALUE          = 0x10004
    GLFW_OUT_OF_MEMORY          = 0x10005
    GLFW_API_UNAVAILABLE        = 0x10006
    GLFW_VERSION_UNAVAILABLE    = 0x10007
    GLFW_PLATFORM_ERROR         = 0x10008
    GLFW_FORMAT_UNAVAILABLE     = 0x10009

    GLFW_FOCUSED                = 0x20001
    GLFW_ICONIFIED              = 0x20002
    GLFW_RESIZABLE              = 0x20003
    GLFW_VISIBLE                = 0x20004
    GLFW_DECORATED              = 0x20005

    GLFW_RED_BITS               = 0x21001
    GLFW_GREEN_BITS             = 0x21002
    GLFW_BLUE_BITS              = 0x21003
    GLFW_ALPHA_BITS             = 0x21004
    GLFW_DEPTH_BITS             = 0x21005
    GLFW_STENCIL_BITS           = 0x21006
    GLFW_ACCUM_RED_BITS         = 0x21007
    GLFW_ACCUM_GREEN_BITS       = 0x21008
    GLFW_ACCUM_BLUE_BITS        = 0x21009
    GLFW_ACCUM_ALPHA_BITS       = 0x2100A
    GLFW_AUX_BUFFERS            = 0x2100B
    GLFW_STEREO                 = 0x2100C
    GLFW_SAMPLES                = 0x2100D
    GLFW_SRGB_CAPABLE           = 0x2100E
    GLFW_REFRESH_RATE           = 0x2100F

    GLFW_CLIENT_API             = 0x22001
    GLFW_CONTEXT_VERSION_MAJOR  = 0x22002
    GLFW_CONTEXT_VERSION_MINOR  = 0x22003
    GLFW_CONTEXT_REVISION       = 0x22004
    GLFW_CONTEXT_ROBUSTNESS     = 0x22005
    GLFW_OPENGL_FORWARD_COMPAT  = 0x22006
    GLFW_OPENGL_DEBUG_CONTEXT   = 0x22007
    GLFW_OPENGL_PROFILE         = 0x22008

    GLFW_OPENGL_API             = 0x30001
    GLFW_OPENGL_ES_API          = 0x30002

    GLFW_NO_ROBUSTNESS          = 0
    GLFW_NO_RESET_NOTIFICATION  = 0x31001
    GLFW_LOSE_CONTEXT_ON_RESET  = 0x31002

    GLFW_OPENGL_ANY_PROFILE     = 0
    GLFW_OPENGL_CORE_PROFILE    = 0x32001
    GLFW_OPENGL_COMPAT_PROFILE  = 0x32002

    GLFW_CURSOR                 = 0x33001
    GLFW_STICKY_KEYS            = 0x33002
    GLFW_STICKY_MOUSE_BUTTONS   = 0x33003

    GLFW_CURSOR_NORMAL          = 0x34001
    GLFW_CURSOR_HIDDEN          = 0x34002
    GLFW_CURSOR_DISABLED        = 0x34003

    GLFW_CONNECTED              = 0x40001
    GLFW_DISCONNECTED           = 0x40002

    def self.load_lib( lib = nil, path = nil )
        lib, path = case RbConfig::CONFIG['host_os']
            when /mac\s?os|darwin/ix
                ['libglfw.dylib', '/System/Library/Frameworks']
            when /mswin|msys|mingw|cygwin|bcwin|wince|emc/ix
                ['glfw3.dll', 'C:/Windows/System32']
            when /linux|unix|bsd/ix
                ['libglfw.so', nil ]
        end unless lib

        dlload( path ? path + '/' + lib : lib )
        import_symbols
    end

    def self.extern( signature, *opts )
        sym, returns, args = parse_signature( signature, @type_alias )
        name = sym.gsub( /@.+/, '')
        opt = parse_bind_options( opts )
        fptr = import_function( sym, returns, args, opt[ :call_type ]) rescue procaddr( name, returns, args )

        # get invocation location
        file, line = caller.first.scan( /^(.+?):(\d+)/ ).map {|(f, l)| [f, l ? l.to_i - 2 : nil] }.flatten

        # register function
        GLFW_FUNCTIONS_MAP[ name.to_sym ] = fptr
        module_eval( %Q{
            def #{name}( *args, &block )
                GLFW_FUNCTIONS_MAP[ :#{name} ].call( *args, &block )
            end
        }, file || __FILE__, line || __LINE__ + 3 )
        module_function( name )

        # return pointer
        fptr
    end

    def self.[]( name )
        GLFW_FUNCTIONS_MAP[ name ]
    end

    def self.create_callback( sym, proc = nil, &block )
        block ? bind( @@callback_extern[ sym ], nil, &block ) : bind( @@callback_extern[ sym ], nil, &proc )
    end

    def self.import_symbols()
        extern 'int glfwInit()'
        extern 'void glfwTerminate()'
        extern 'void glfwGetVersion( int*, int*, int* )'
        extern 'const char* glfwGetVersionString()'
        extern 'void* glfwSetErrorCallback( void* )'
        extern 'void** glfwGetMonitors( int* )'
        extern 'void* glfwGetPrimaryMonitor()'
        extern 'void glfwGetMonitorPos( void*, int*, int* )'
        extern 'void glfwGetMonitorPhysicalSize( void*, int*, int* )'
        extern 'const char* glfwGetMonitorName( void* )'
        extern 'void* glfwSetMonitorCallback( void* )'
        extern 'const void* glfwGetVideoModes( void*, int* )'
        extern 'GLFWvidmode* glfwGetVideoMode( void* )'
        extern 'void glfwSetGamma( void*, float )'
        extern 'GLFWgammaramp* glfwGetGammaRamp( void* )'
        extern 'void glfwSetGammaRamp( void*, const void* )'
        extern 'void glfwDefaultWindowHints()'
        extern 'void glfwWindowHint( int, int )'
        extern 'void* glfwCreateWindow( int, int, const char*, void*, void* )'
        extern 'void glfwDestroyWindow( void* )'
        extern 'int glfwWindowShouldClose( void* )'
        extern 'void glfwSetWindowShouldClose( void* window, int )'
        extern 'void glfwSetWindowTitle( void*, const char*)'
        extern 'void glfwGetWindowPos( void*, int*, int* )'
        extern 'void glfwSetWindowPos( void*, int, int )'
        extern 'void glfwGetWindowSize( void*, int*, int* )'
        extern 'void glfwSetWindowSize( void*, int, int )'
        extern 'void glfwGetFramebufferSize( void*, int*, int* )'
        extern 'void glfwIconifyWindow( void* )'
        extern 'void glfwRestoreWindow( void* )'
        extern 'void glfwShowWindow( void* )'
        extern 'void glfwHideWindow( void* )'
        extern 'void* glfwGetWindowMonitor( void* )'
        extern 'int glfwGetWindowAttrib( void*, int )'
        extern 'void glfwSetWindowUserPointer( void*, void* )'
        extern 'void* glfwGetWindowUserPointer( void* )'
        extern 'void* glfwSetWindowPosCallback( void*, void* )'
        extern 'void* glfwSetWindowSizeCallback( void*, void* )'
        extern 'void* glfwSetWindowCloseCallback( void*, void* )'
        extern 'void* glfwSetWindowRefreshCallback( void*, void* )'
        extern 'void* glfwSetWindowFocusCallback( void*, void* )'
        extern 'void* glfwSetWindowIconifyCallback( void*, void* )'
        extern 'void* glfwSetFramebufferSizeCallback( void*, void* )'
        extern 'void glfwPollEvents()'
        extern 'void glfwWaitEvents()'
        extern 'int glfwGetInputMode( void*, int )'
        extern 'void glfwSetInputMode( void*, int, int )'
        extern 'int glfwGetKey( void*, int )'
        extern 'int glfwGetMouseButton( void*, int )'
        extern 'void glfwGetCursorPos( void*, double*, double* )'
        extern 'void glfwSetCursorPos( void*, double, double )'
        extern 'void* glfwSetKeyCallback( void*, void* )'
        extern 'void* glfwSetCharCallback( void*, void* )'
        extern 'void* glfwSetMouseButtonCallback( void*, void* )'
        extern 'void* glfwSetCursorPosCallback( void*, void* )'
        extern 'void* glfwSetCursorEnterCallback( void*, void* )'
        extern 'void* glfwSetScrollCallback( void*, void* )'
        extern 'int glfwJoystickPresent( int )'
        extern 'const float* glfwGetJoystickAxes( int, int* )'
        extern 'const unsigned char* glfwGetJoystickButtons( int, int* )'
        extern 'const char* glfwGetJoystickName( int )'
        extern 'void glfwSetClipboardString( void*, const char* )'
        extern 'const char* glfwGetClipboardString( void* )'
        extern 'double glfwGetTime()'
        extern 'void glfwSetTime(double)'
        extern 'void glfwMakeContextCurrent( void* )'
        extern 'void* glfwGetCurrentContext()'
        extern 'void glfwSwapBuffers( void* )'
        extern 'void glfwSwapInterval( int )'
        extern 'int glfwExtensionSupported( const char* )'
        extern 'void* glfwGetProcAddress( const char* )'
        true
    end
end
