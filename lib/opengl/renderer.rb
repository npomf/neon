# Programmable renderloop thread.  Operates on the current Window instance and invokes the block given to render each
# frame.  Includes timing options.
class Renderer < Thread

    # Current singleton instance
    @@instance = nil
    # Renderer paused flag
    attr_writer :paused
    # Total frames rendered
    attr_reader :frames
    # Frame limiter value, in FPS
    attr_accessor :throttle
    # Internal frame timings
    attr :timings
    # Underlying rendering procedure
    attr :func

    # Initialize the renderer with a custom rendering operation.
    def initialize( fps = nil, &block )
        @paused = false
        @throttle = fps
        @func = block

        # Setup runtime
        super {
            glfwMakeContextCurrent( Window.instance.pointer )
            loop {
                sleep 0.1 until @func
                while Window.instance.minimized? or @paused
                    sleep 0.1
                    glfwPollEvents()
                end

                # Render the frame, track the timing
                track { @func.call rescue nil }

                # Event automation and buffer handling
                glfwSwapBuffers( Window.instance.pointer )
                glfwPollEvents()
                break unless glfwWindowShouldClose( Window.instance ).eql? 0
            }

            # Clean up
            glfwDestroyWindow( Window.instance.pointer )
            Window.instance = nil
        }
    end

    # Override the rendering procedure.
    def func=( proc = nil, &block )
        @func = proc || block
    end

    # Compute the current FPS using the average frame time.
    def fps()
        1000.0 / (@timings.inject( :+ ) / @timings.length)
    end

    # Check if the renderer is paused.
    def paused?()
        @paused
    end

    # Handles tracking of frames.  If throttling is enabled (non-nil, >0), this method will handle that as well.
    def track( &block )
        head = Time.now
        block.call()
        delta = Time.now - head

        # Handle throttling
        if @throttle and @throttle > 0
            target = 1000.0 / @throttle
            diff = target - delta
            idle = Time.now
            sleep( diff / 1000.0 )
            delta += Time.now - idle
        end

        @frames += 1
        @timings << delta
        @timings.shift if @timings.length > 60
    end
    private :track

    # Create instance.
    def self.new( *args, **kwargs )
        return @@instance unless @@instance.nil?
        obj = super( *args, **kwargs )
        @@instance = obj
    end

    # Create instance.
    def self.new( *args, **kwargs, &block )
        return @@instance unless @@instance.nil?
        obj = super( *args, **kwargs, &block )
        @@instance = obj
    end
end
