require 'fiddle'
require_relative 'camera'
require_relative 'renderer'
require_relative '../events'
require_relative '../inputs'

# Window object.
class Window

    # Singleton instance
    @@instance = nil
    # Current GLFW window pointer
    attr :pointer
    # Focus flag
    attr :focused
    # Minimized flag
    attr :minimized
    # Current camera contex
    attr_accessor :camera
    # Mouse or joystic binding
    attr_accessor :mouse
    # Keyboard binding
    attr_accessor :keyboard

    def initialize( title, width: 0, height: 0, borderless: false, on_top: false, fullscreen: false, multisample: 0 )
        glfwDefaultWindowHints()
        glfwWindowHint( GLFW_DECORATED, 0 ) if borderless or fullscreen
        glfwWindowHint( GLFW_FLOATING, 1 ) if on_top
        glfwWindowHint( GLFW_SAMPLES, multisample )

        # get screen dimensions, create window
        vidmode = Window.monitor()
        dim_x, dim_y = vidmode['width'], vidmode['height']
        @pointer = glfwCreateWindow(
            width > 0 ? width : dim_x,
            height > 0 ? height : dim_y,
            title,
            fullscreen ? glfwGetMonitors() : nil,   # span all monitors if fullscreen
            nil
        )

        # position it
        glfwSetWindowPos( @pointer, (dim_x - width) / 2, (dim_y - height) / 2 )

        # set callbacks
        on_key {|win, key, code, action, mods|
            close if (key.eql? GLFW_KEY_ESCAPE and (mods & GLFW_MOD_SHIFT).eql? GLFW_MOD_SHIFT)
            @keyboard.key( key, action, code, mods )
        }
        on_click {|win, button, action, mods| @mouse.button( button, action, mods ) }
        on_move {|win, x, y| @mouse.move( x, y ) }
        on_scroll {|win, x, y| @mouse.scroll( x, y ) }
        on_focus {|win, state| emit( (@focused = state.eql? 1) ? :window_focus_gained : :window_focus_lost ) }
        on_iconify {|win, state| emit( (@minimized = state.eql? 1) ? :window_minimized : :window_restored ) }
        on_resize {|win, width, height|
            emit( :surface_resized, width, height )
            @camera.viewport = width, height if @camera
        }
        on_close {|win|
            close
            Neon.terminate
        }

        # Make it current!
        glfwMakeContextCurrent( @pointer )
    end

    # Set the key callback.
    def on_key( &block )
        glfwSetKeyCallback( @pointer, GLFW::create_callback( :GLFWkeyfun, &block ) )
    end

    # Set the mouse button callback.
    def on_click( &block )
        glfwSetMouseButtonCallback( @pointer, GLFW::create_callback( :GLFWmousebuttonfun, &block ) )
    end

    # Set the mouse movement callback.
    def on_move( &block )
        glfwSetCursorPosCallback( @pointer, GLFW::create_callback( :GLFWcursorposfun, &block ) )
    end

    # Set the scroll callback.
    def on_scroll( &block )
        glfwSetScrollCallback( @pointer, GLFW::create_callback( :GLFWscrollfun, &block ) )
    end

    # Set the focus change callback.
    def on_focus( &block )
        glfwSetWindowFocusCallback( @pointer, GLFW::create_callback( :GLFWwindowfocusfun, &block ) )
    end

    # Set the iconification change callback.
    def on_iconify( &block )
        glfwSetWindowIconifyCallback( @pointer, GLFW::create_callback( :GLFWwindowiconifyfun, &block ) )
    end

    # Set the resize callback.
    def on_resize( &block )
        glfwSetWindowSizeCallback( @pointer, GLFW::create_callback( :GLFWwindowsizefun, &block ) )
    end

    # Set the closing callback.
    def on_close( &block )
        glfwSetWindowCloseCallback( @pointer, GLFW::create_callback( :GLFWwindowclosefun, &block ) )
    end

    # Close the window.
    def close()
        emit( :window_closing )
        glfwSetWindowShouldClose( @pointer, 1 )
    end

    # Destroy the context.
    def destroy()
        close
        emit( :window_destroyed )
        glfwDestroyWindow( @pointer )
        @@instance = nil
    end

    # Check if the window has focus.
    def focused?()
        @focused
    end

    # Check if the window is minimized.
    def minimized?()
        @minimized
    end
    alias :hidden? :minimized?

    # Get current instance.
    def self.instance()
        @@instance
    end

    # Acquire the monitor metrics via GLFW.
    def self.monitor()
        vidmode = Fiddle::CStructEntity.new(
            glfwGetVideoMode( glfwGetPrimaryMonitor() ),
            [ Fiddle::TYPE_INT,
              Fiddle::TYPE_INT,
              Fiddle::TYPE_INT,
              Fiddle::TYPE_INT,
              Fiddle::TYPE_INT,
              Fiddle::TYPE_INT ]
        )
        vidmode.assign_names(['width', 'height', 'redBits', 'greenBits', 'blueBits', 'refreshRate'])
        vidmode
    end

    # Create instance.
    def self.new( *args, **kwargs )
        return @@instance unless @@instance.nil?
        obj = super( *args, **kwargs )
        @@instance = obj
    end
end
