require_relative '../ext/safely'
require_relative '../ext/arithmetic'
require_relative '../openal/equalizer'
require_relative '../openal/listener'
require_relative '../openal/sound'

# Audio mixing process thread, which holds active Sound instances and manages their playback.
#
# Use Listener to position the receiver for rendered sound playback.  It is independent of the Mixer.
class Mixer < Thread

    # Singleton instance
    @@instance = nil
    # Bank of sounds
    attr_reader :sounds

    # Initialize the mixer.
    def initialize()
        @sounds = []
        super(){ runtime }
    end

    # Add a sound to the mixer.
    def add( sound )
        @sounds << sound
    end

    # Clear the mixer of all sounds.  Set `destroy` to true to also clean them up.
    def clear( destroy = false )
        @sounds.each {|sound| remove( sound, destroy ) }
        self
    end

    # Pause or resume **all** sounds.
    def play( enabled = true )
        @sounds.each {|sound| sound.send( enabled ? :play : :pause ) }
        self
    end

    # Remove a sound from the mixer; an index may be passed as well.  Set `destroy` to true to also clean up the
    # sound on removal.
    def remove( sound, destroy = false )
        sound = @sounds[ sound ] if sound.is_a? Fixnum
        @sounds.delete( sound )
        sound.destroy! if destroy
        sound
    end

    # Clear the mixer and end its runtime.
    def close()
        clear
        kill unless join( 0.5 ) # Force death in 1/2 second
    end

    # Mixer runtime.
    def runtime()
        safely {
            loop {
                sleep 0.1 while @sounds.empty?
                Equalizer.update
                @sounds.each {|sound|
                    next unless sound.playing?
                    next if Listener.location.xyz.distance( sound.location.xyz ) > Sound.max_distance

                    # Update listener, stream next chunk of sound; remove and dispose if it has ended
                    Listener.update
                    remove( sound, true ) unless sound.stream
                }
            }
        }
        clear
    end
    protected :runtime

    # Get current instance.
    def self.instance()
        @@instance
    end

    # Create instance.
    def self.new( *args, **kwargs )
        return @@instance unless @@instance.nil?
        obj = super( *args, **kwargs )
        @@instance = obj
    end
end
