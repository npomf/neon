# Soundscape acoustic presets
# ---------------------------
#
# This file includes reverberation presets as specified in OpenAL Soft's EFX module, which is available
# here:  https://raw.githubusercontent.com/kcat/openal-soft/master/include/AL/efx-presets.h
#
# Each preset is a constant configured to represent the acoustic qualities of a locale.  As they are non-standard to the
# default implementation of the Soundscape, they have been separated into this file to keep the code cleaner.  Simply
# require it to expand the Soundscape class to include these new presets.

require_relative '../scene/soundscape'

# Padded Cell
Soundscape::PADDED_CELL = Soundscape.from(
    0.1715, 1.0, 0.3162, 0.001, 0.17, 0.1, 0.25, 0.001, 1.2691, 0.002, 0.9943, 0.0, 1
)

# Room
Soundscape::ROOM = Soundscape.from(
    0.4287, 1.0, 0.3162, 0.5929, 0.4, 0.83, 0.1503, 0.002, 1.0629, 0.003, 0.9943, 0.0, 1
)

# Bathroom
Soundscape::BATHROOM = Soundscape.from(
    0.1715, 1.0, 0.3162, 0.2512, 1.49, 0.54, 0.6531, 0.007, 3.2734, 0.011, 0.9943, 0.0, 1
)

# Living Room
Soundscape::LIVINGROOM = Soundscape.from(
    0.9766, 1.0, 0.3162, 0.001, 0.5, 0.1, 0.2051, 0.003, 0.2805, 0.004, 0.9943, 0.0, 1
)

# Stone Room
Soundscape::STONE_ROOM = Soundscape.from(
    1.0, 1.0, 0.3162, 0.7079, 2.31, 0.64, 0.4411, 0.012, 1.1003, 0.017, 0.9943, 0.0, 1
)

# Auditorium
Soundscape::AUDITORIUM = Soundscape.from(
    1.0, 1.0, 0.3162, 0.5781, 4.32, 0.59, 0.4032, 0.02, 0.717, 0.03, 0.9943, 0.0, 1
)

# Concert Hall
Soundscape::CONCERT_HALL = Soundscape.from(
    1.0, 1.0, 0.3162, 0.5623, 3.92, 0.7, 0.2427, 0.02, 0.9977, 0.029, 0.9943, 0.0, 1
)

# Cave
Soundscape::CAVE = Soundscape.from(
    1.0, 1.0, 0.3162, 1.0, 2.91, 1.3, 0.5, 0.015, 0.7063, 0.022, 0.9943, 0.0, 0
)

# Arena
Soundscape::ARENA = Soundscape.from(
    1.0, 1.0, 0.3162, 0.4477, 7.24, 0.33, 0.2612, 0.02, 1.0186, 0.03, 0.9943, 0.0, 1 
)

# Hangar
Soundscape::HANGAR = Soundscape.from(
    1.0, 1.0, 0.3162, 0.3162, 10.05, 0.23, 0.5, 0.02, 1.256, 0.03, 0.9943, 0.0, 1 
)

# Carpeted Hallway
Soundscape::CARPETED_HALLWAY = Soundscape.from(
    0.4287, 1.0, 0.3162, 0.01, 0.3, 0.1, 0.1215, 0.002, 0.1531, 0.03, 0.9943, 0.0, 1
)

# Hallway
Soundscape::HALLWAY = Soundscape.from(
    0.3645, 1.0, 0.3162, 0.7079, 1.49, 0.59, 0.2458, 0.007, 1.6615, 0.011, 0.9943, 0.0, 1
)

# Stone Corridor
Soundscape::STONE_CORRIDOR = Soundscape.from(
    1.0, 1.0, 0.3162, 0.7612, 2.7, 0.79, 0.2472, 0.013, 1.5758, 0.02, 0.9943, 0.0, 1
)

# Alley
Soundscape::ALLEY = Soundscape.from(
    1.0, 0.3, 0.3162, 0.7328, 1.49, 0.86, 0.25, 0.007, 0.9954, 0.011, 0.9943, 0.0, 1
)

# Forest
Soundscape::FOREST = Soundscape.from(
    1.0, 0.3, 0.3162, 0.0224, 1.49, 0.54, 0.0525, 0.162, 0.7682, 0.088, 0.9943, 0.0, 1
)

# City
Soundscape::CITY = Soundscape.from(
    1.0, 0.5, 0.3162, 0.3981, 1.49, 0.67, 0.073, 0.007, 0.1427, 0.011, 0.9943, 0.0, 1
)

# Mountains
Soundscape::MOUNTAINS = Soundscape.from(
    1.0, 0.27, 0.3162, 0.0562, 1.49, 0.21, 0.04, 0.3, 0.1919, 0.1, 0.9943, 0.0, 0
)

# Quarry
Soundscape::QUARRY = Soundscape.from(
    1.0, 1.0, 0.3162, 0.3162, 1.49, 0.83, 0.0, 0.061, 1.7783, 0.025, 0.9943, 0.0, 1
)

# Plain
Soundscape::PLAIN = Soundscape.from(
    1.0, 0.21, 0.3162, 1.0, 1.49, 0.5, 0.0585, 0.179, 0.1089, 0.1, 0.9943, 0.0, 1
)

# Parking Lot
Soundscape::PARKING_LOT = Soundscape.from(
    1.0, 1.0, 0.3162, 1.0, 1.65, 1.5, 0.2082, 0.008, 0.2652, 0.012, 0.9943, 0.0, 0
)

# Sewer Pipe
Soundscape::SEWER_PIPE = Soundscape.from(
    0.3071, 0.8, 0.3162, 0.3162, 2.81, 0.14, 1.6387, 0.014, 3.2471, 0.021, 0.9943, 0.0, 1
)

# Underwater
Soundscape::UNDERWATER = Soundscape.from(
    0.3645, 1.0, 0.3162, 0.01, 1.49, 0.1, 0.5963, 0.007, 7.0795, 0.011, 0.9943, 0.0, 1
)

# Drugged
Soundscape::DRUGGED = Soundscape.from(
    0.4287, 0.5, 0.3162, 1.0, 8.39, 1.39, 0.876, 0.002, 3.1081, 0.03, 0.9943, 0.0, 0
)

# Dizzy
Soundscape::DIZZY = Soundscape.from(
    0.3645, 0.6, 0.3162, 0.631, 17.23, 0.56, 0.1392, 0.02, 0.4937, 0.03, 0.9943, 0.0, 0
)

# Psychotic
Soundscape::PSYCHOTIC = Soundscape.from(
    0.0625, 0.5, 0.3162, 0.8404, 7.56, 0.91, 0.4864, 0.02, 2.4378, 0.03, 0.9943, 0.0, 0
)
