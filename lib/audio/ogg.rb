require_relative 'decoder'
require_relative '../ext/dstruct'

# Provides Ogg (.ogg) and Vorbis handling and playback.
module Ogg

    # Default signature / capture sequence
    OGG_SIGNATURE = 'OggS'
    # Default vorbis signature
    VORBIS_SIGNATURE = 'vorbis'

    # Ogg file decoder class.
    class Decoder
        include AudioDecoder

        # Underlying IO stream
        attr :io
        # Packet data cache
        attr :packets
        # Partial packet buffer
        attr :buffer
        # Identification header
        attr_reader :header
        # Comments
        attr_reader :comments

        # Initialize against a filepath or IO.
        def initialize( target )
            @io = (target.is_a? IO) ? target : open( target )
            @packets = []
            @buffer = ''

            # Set up headers
            @header = Header.read( packet() )
            @comments = Comments.read( packet() ).data
        end

        # Get sample rate.
        def sample_rate()
            @header.sample_rate
        end

        # Get number of channels.
        def channels()
            @header.channels
        end

        # Retrieve the audio codec.
        def codec()
            @header.codec
        end

        # Close the IO stream.
        def close()
            @io.close
        end

        # Seek to the next available page.
        def seek( capture = Ogg::OGG_SIGNATURE )
            data = @io.read( capture.length )

            until @io.eof?
                return @io.pos -= capture.length if data.eql? capture
                data = data[1 .. -1] << @io.read( 1 ) rescue Exception
            end
            nil # Error
        end

        # Seek to and read the next page.
        def page()
            Page.read( @io ) if not @io.eof? and seek
        end
        alias :next_page :page

        # Seek to and read the next contiguous packet, otherwise nil.
        def packet()
            while @packets.empty?
                pg = page()
                return nil unless pg    # End of Stream

                data = StringIO.new( pg.data )
                page.segment_table.each {|segment|
                    @buffer << data.read( segment )
                    unless segment.eql? 255
                        @packets.insert( 0, @buffer )
                        @buffer = ''    # Flush
                    end
                }
            end
            @packets.pop
        end
        alias :next_packet :packet

        # Read next packet as PCM data.  This opeartion decodes from the Vorbis codec and assumes default properties of
        # a 16-bit word and no Big-Endian enforcement (will test host for compliance) in a channel-agnostic manner.
        #
        # *Note:  defined from [ov_read_filter](https://git.xiph.org/?p=vorbis.git;a=blob;f=lib/vorbisfile.c)*
        def next()
            raw = packet()
            big_e = IO.big_endian?
            offset = 1 ? 0 : 32768 # use signed how ?

            # Read all samples
            pcm = []
            block.unpack( big_e ? 'g' : 'e').each {|sample|
                sample *= 32768.0
                if sample > 32767
                    sample = 32767
                elsif sample < -32768
                    sample = -32768
                end
                sample += offset

                # Convert and append
                if big_e
                    pcm << (sample >> 8)
                    pcm << sample & 0xff
                else
                    pcm << sample & 0xff
                    pcm << (sample >> 8)
                end
            }
            pcm.pack('s*')
        end
    end

    # An Ogg Page.
    class Page
        include DataStruct
        endian :little

        # OggS capture assertion
        field :capture, :chars, length: 4, assert: Ogg::OGG_SIGNATURE
        # Version number
        field :version, :ubyte
        # Ogg page type
        field :type, :ubyte
        # Granule position
        field :granule_position, :ulong
        # Serial number of bitstream
        field :bitstream_serial_number, :uint
        # Page sequencing number (index)
        field :sequence_number, :uint
        # Page data checksum
        field :checksum, :uint
        # Number of segments in this page
        field :segments, :ubyte
        # Page segmentation table
        field :segment_table, :ubyte, length: :segments
        # Page data
        field :data, ->(io){ @segment_table.inject( 0 ){|t, e| t + e } }
    end

    # An Ogg Identification Header.
    class Header
        include DataStruct
        endian :little

        # Page type
        field :type, :ubyte
        # Vorbis signature
        field :signature, :chars, length: 6, assert: Ogg::VORBIS_SIGNATURE
        # Version number
        field :version, :short
        # Number of audio channels
        field :channels, :ubyte
        # Audio sample rate
        field :sample_rate, :uint
        # Bit rate minimum
        field :bitrate_min, :int
        # Nominal bit rate
        field :bitrate, :int
        # Bit rate maximum
        field :bitrate_max, :int
        # Block size 0
        field :blocksize_0, ->(io){ io.read_ubyte & 0x0f; io.seek( -1, IO::SEEK_CUR ) }
        # Block size 1
        field :blocksize_1, ->(io){ io.read_ubyte & 0x0f; io.seek( -1, IO::SEEK_CUR ) }
    end

    # Comments block.
    class Comments
        include DataStruct
        endian :little

        # Page type
        field :type, :ubyte
        # Vorbis signature
        field :signature, :chars, length: 6, assert: Ogg::VORBIS_SIGNATURE
        # Vendor string length
        field :vendor_name_length, :uint
        # Vendor name
        field :vendor, :chars, length: :vendor_name_length
        # Number of comment entries
        field :length, :uint
        # Comment data
        field :data, ->(io){
            @data = {}
            @length.times {
                body = io.read( io.read_uint )
                name, comment = body.split('=', 2 )
                @data[ name.downcase ] = comment
            }
        }
    end
end
