# Abstract audio file decoder.
module AudioDecoder

    # Return the number of channels.
    def channels(); end

    # Return the sample rate of the audio or current block.
    def sample_rate(); end

    # Return the next available block of PCM data as a String, or nil if the end of the stream has been reached.
    def next(); end

    # Close the underlying IO stream.
    def close(); end
end
