require 'thread_safe'
require 'set'
require 'msgpack'

# Container for caching and tracking of loaded resources and objects within the system.  Objects can be tagged to allow
# for easier discovery and grouping.
module Objects
    extend self
    
    # Object cache
    attr :cache
    @cache = ThreadSafe::Cache.new()
    
    # Object tags
    attr :tags
    @tags = ThreadSafe::Cache.new()
    
    # Create a V4-like psuedo-random UUID.
    def uuid()
        "%08x-%04x-%04x-%04x-%04x%08x" % gen_id( 128 ).unpack('NnnnnN')
    end
    
    # Generate an N-bit psuedo-random identifier string.
    def gen_id( bits = 128 )
        Array.new( (bits / 8).to_i ){ rand( 256 ).chr }.join
    end
    
    # Perform a diff against the current list of object IDs and the provided manifest, returning an array of all IDs
    # which *do not* appear in the provided manifest.
    #
    # These results can then be passed to the #delete method to remove them, such as when a new scene should be loaded
    # and old resources discarded.
    def diff( manifest )
        @cache.keys - manifest
    end
    
    # Delete an object.  This will remove it from the cache and delete any associated tags as well.
    def delete( *ids )
        ids.each {|id|
            next unless @cache.key? id
            obj = @cache.delete id
            @tags.values.each {|tag| tag.delete id }
            obj.destroy! if obj.respond_to? :destroy!
        }
    end
    alias :unload :delete

    # Load and cache a new object.  Returns the loaded object.
    def load( io )
        obj = MessagePack.load( (io.is_a? IO) ? io.read : io )
        cache( obj )
        obj
    end

    # Dump an object.  If `io` is specified, the results will be written to the stream.
    def dump( id, io = nil )
        return unless exists? id
        obj = object( id )
        out = { cache_id: @cache_id, object: obj }.to_msgpack
        io.write( out ) if io
        out
    end
    alias :save :dump
    
    # Cache an object.  Returns the id of the object on insertion, false otherwise.
    def cache( id = nil, obj )
        id ||= obj.id
        return false if @cache.key? id or id.nil?
        @cache[ id ] = obj
        id
    end
    
    # Uncache an object.
    def uncache( id )
        @cache.delete id
    end
    
    # Clear the object cache and all tags.
    def clear_cache()
        @cache.clear
        @tags.clear
    end

    # Check if an object exists.
    def exists?( id )
        @cache.key? id
    end
    
    # Get an object by its ID.
    def object( id )
        @cache[ id ]
    end
    
    # Get all object IDs by a given tag as an array.
    def tagged( tag )
        @tags[ tag ].to_a
    end
    
    # Check if an object ID has been tagged.
    def tagged?( id, tag )
        @tags[ tag ].include? id
    end
    
    # Get all objects whose IDs are associated ot the given tag as an array.
    def with( tag )
        tagged( tag ).map {|id| object( id ) }
    end
    
    # Tag an object ID.
    def tag( id, *tags )
        tags.each {|tag| (@tags[ tag ] ||= Set.new) << id }
    end
    
    # Untag an object ID.
    def untag( id, *tags )
        tags.each {|tag| @tags[ tag ].delete( id ) if @tags.key? tag }
    end
    
    # Count all objects of the given type.
    def count( type )
        @cache.values.select {|obj| obj.is_a? type }.length
    end
    
    # Cache ID (used when acting as a data peer/host)
    attr_reader :cache_id
    @cache_id = uuid()
end

# The ReferTo module is a dummy psuedoclass used to intercept object loading.  Its only parameter is the identifier of
# the already cached object to be loaded.  This methodology works best when resources are preloaded.
module ReferTo

    # Intercepts the default load operations, checks the object cache, and returns a clone of the loaded resource.  All 
    # additional metadata is discarded.
    #
    # A `LoadError` is raised if no object with a matching identifier is found in the catch.
    def self.msgpack_create( id:, **metadata )
        basis = Objects.object( id )
        raise LoadError, "ReferTo for '#{id}' failed; no resource with that ID was found in the cache!" unless basis
        basis.clone
    end

    # Blow up.  You're not supposed to include this!
    def self.included( o )
        raise 'The module ReferTo cannot be included.'
    end
end

# Load all underlying object definitions
Dir.glob('lib/object/*.rb').each {|lib| require "./#{lib}" }
