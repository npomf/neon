# Import dependencies
require 'matrix'
require_relative 'arithmetic'

# Expand the Matrix class
Matrix.class_eval %q{

    # MessagePack serialization.
    def to_msgpack()
        { MessagePack.create_id => self.class.name, data: to_a }.to_msgpack
    end

    # MessagePack deserialization.
    def self.msg_create( obj )
        Matrix[ *(obj['data']) ]
    end

    # Get the row and column size of this matrix, as an array.
    def dimensions()
        [row_size, column_size]
    end

    # Iterate over all elements in the matrix with their row and column positions.
    def iterate( &block )
        row_size.times {|r| column_size.times {|c| block.call( r, c, self[ r, c ] ) } }
        self
    end
    alias :each_with_index :iterate

    alias :width :row_size
    alias :height :column_size
}

# Expand the Vector class
Vector.class_eval %q{

    # MessagePack serialization.
    def to_msgpack()
        { MessagePack.create_id => self.class.name, data: to_a }.to_msgpack
    end

    # MessagePack deserialization.
    def self.msg_create( obj )
        Vector.elements( obj['data'] )
    end

    # Get the X component.
	def x()
		self[0]
	end
	alias :r :x
	alias :red :x

	# Set the X component.
	def x=( v )
		self[0] = v
	end
	alias :r= :x=
	alias :red= :x=

	# Get the Y component.
	def y()
		self[1]
	end
	alias :g :y
	alias :green :y

	# Set the Y component.
	def y=( v )
		self[1] = v
	end
	alias :g= :y=
	alias :green= :y=

	# Get the Z component.
	def z()
		self[2]
	end
	alias :b :z
	alias :blue :z

	# Set the Z component.
	def z=( v )
		self[2] = v
	end
	alias :b= :z=
	alias :blue= :z=

	# Get the W component.
	def w()
		self[3]
	end
	alias :a :w
	alias :alpha :w

	# Set the W component.
	def w=( v )
		set( 3, v )
	end
	alias :a= :w=
	alias :alpha= :w=

	# Get the XYZ components as a vec3.
	def xyz()
		Vector[ x(), y(), z() ]
	end
	alias :rgb :xyz

	# Set one or more of the XYZ[W] components of this vector as a vec4.
	def set( *axis )
	    axis.each_with_index {|el, i| self[i] = el }
	    self
	end

    # Return the projection of this vector onto another.
    def projection( vec )
        ( vec.dot( self ) / vec.r ) * vec
    end

	# Get the pythagorean distance between the endpoints of two vector's XYZ components.
	def distance( v2 )
	    Math.sqrt( (self - v2).xyz.to_a.map {|c| Math.pow( c, 2 ) }.inject( :+ ) )
	end

	# Obtain the cross product of this vector and another.
	def cross_product( v )
	    Vector[
	        self.y * v.z - self.z * v.y,
	        self.z * v.x - self.x - v.z,
	        self.x * v.y - self.y * v.x
	    ]
	end
	alias :cross :cross_product

	# Obtain a vector pointing in the direction of reflection.
	def reflect( n )
	    m = n.normalize
	    normalize - (2 * dot( m ) * m)
	end

	# Obtain a vector pointing in the direction of refraction using the `ratio` of indices of refraction.
	def refract( n, ratio = 1.0 )
	    m = n.normalize
	    d = normalize.dot( m )
        k = 1.0 - (ratio * ratio * (1.0 - d * d))
        k < 0 ? 0 : ratio * normalize - (ratio * d + sqrt( k )) * m
	end

    alias :dot :inner_product
    alias :! :-@
}
