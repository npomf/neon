Hash.class_eval( %q{

	# This allows JS-style property accessing by invoking the key as a virtual method.
	def method_missing( sym, *params, &block )
		_, raw, setter = sym.to_s.match( /^([\w\d_]+)(=?)$/ ).to_a
		if not setter.empty?
		    self[ raw ] = params.length == 1 ? params[0] : params
		    return *params
		elsif keys.include? raw
			unwrap( raw, *params, &block )
		elsif keys.include? raw.to_sym
			unwrap( raw.to_sym, *params, &block )
		else
			nil	# super
		end
	end

	# Attempt to retrieve a property in any of the standard forms.
	def get( key )
		self[ key.to_sym ] || self[ key.to_s ] || self[ key ]
	end

	# Search for a property anywhere within this hash or its child branches.
	def search( sym )
		return self[sym] if (keys.include? sym)
		values.select {|v| v.is_a? Hash }.each {|h|
			r = h.search( sym )
			return r if r
		}
		nil
	end
	alias :scan :search

	# Dynamically populate a tree of hash objects using a canonical path where the terminating element is the leaf (key)
	# to be assigned.
	def set( branch, value, sep = '.')
		leaf, branches = self, branch.split( sep )
		leaf = ((l = leaf[(b = branches.shift)]) ? l : leaf[b] = Hash.new) while branches.length > 1
		leaf[branches.shift] = value
	end
	
	def unwrap( sym, *params, &block )
	    v = self[sym]
	    (v.is_a? Proc or v.is_a? Method) ? v.call( *params, &block ) : v
	end
	private :unwrap

	# Performs a #map_keys() process to ensure **all** keys are symbols.   This is useful for when a hash represents a
	# series of keyword arguments, but is not generated from Ruby code (such as deserialized JSON).  This process is
	# recursive.
	def parameterize()
		map_keys {|k, v|
			v.parameterize if v.is_a? Hash
			k.to_sym
		}
	end
	alias :to_kwargs :parameterize

	# Map the key-value pairs into a new hash using a block to produce new assignments.
	def map( &block )
		return self unless block
		copy = {}
		each {|k, v|
			k2, v2 = block.call( k, v )
			if !v2
				v2 = k2
				k2 = nil
			end
			copy[ k2 || k ] = v2
		}
		copy
	end
	alias :collect :map

	# Remap the key-value pairs using a block to produce new assignments.
	def map!( &block )
		return self unless block
		each {|k, v|
			k2, v2 = block.call( k, v )
			if !v2
				v2 = k2
				k2 = nil
			end
			delete( k ) if k2
			self[ k2 || k ] = v2
		}
		self
	end
	alias :collect! :map!

	# As Array#map(), but only the values.  The result of the block should be the new value.
	def map_values( &block )
		return self unless block
		copy = {}
		each {|k, v| copy[k] = block.call( k, v ) }
		copy
	end

	# As Array#map(), but only the keys.  The result of the block should be the new key.
	def map_keys( &block )
		return self unless block
		copy = {}
		each {|k, v| copy[ block.call( k, v ) ] = v }
		copy
	end
})
