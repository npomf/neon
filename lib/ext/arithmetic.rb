# Extend the Math class
Math.class_eval %q{
    extend self

    # Power function.
    def pow( n, exponent )
        n ** exponent
    end

	# Clamp a value to the specified limit.
	def clamp( n, limit = 1.0 )
		n > limit ? limit : n
	end

	# Determine the minimum value.
	def min( a, b )
		a < b ? a : b
	end
	alias :minimum :min

	# Determine the mimimum value in an array.
	def least( *vals )
		n = vals[0]
		vals[1 .. vals.length - 1].each {|v| n = min( n, v ) }
		n
	end

	# Determine the maximum value.
	def max( a, b )
		a > b ? a : b
	end
	alias :maximum :max

	# Determine the maximum value in an array.
	def greatest( *vals )
		n = vals[0]
		vals[1 .. vals.length - 1].each {|v| n = max( n, v ) }
		n
	end
	
	# Compute the linear blend of `x` and `y`:  `(x * (1 - a)) + (y * a)`.
	def mix( x, y, a )
	    y * a + (x * (1 - a))
	end
	
	# Returns 0 if `x` is smaller than `edge`, else 1.
	def step( edge, x )
	    x < edge ? 0.0 : 1.0
	end
	
	# Returns 0 if `x` is smaller than `edge0`, 1 if `x` is larger than `edge1`; otherwise, interpolate using Hermite
	# polynomials.
	def smoothstep( edge0, edge1, x )
	    return 0.0 if x < edge0
	    return 1.0 if x > edge1
	    hermite( 1, x )
	end
	
	# TODO Compute a physicist's hermite polynomial.
	def hermite( i, x )
	#   Math.pow( -1, i ) * Math.exp( Math.pow( x, 2 ) / 2 ) * 
	end

	# Convert a degree value to radians.
	def radian( deg )
		deg * (PI / 180.0)
	end
	
	# Convert multiple radian values.
	def radians( *degs )
	    degs.map {|deg| radian( deg ) }
	end

	# Convert a radian value to degrees.
	def degree( rad )
		rad * (180.0 / PI)
	end
	
	# Convert multiple degree values.
	def degrees( *rads )
	    rads.map {|rad| degree( rad ) }
	end
	
	# Compute inverse square root.
	# Uses a modified version of Quake III Arena code.
	def inversesqrt( n )
	    n2 = n * 0.5
	    i = 0x5f3759df - (n.to_i >> 1)
	    f = i.to_i
	    f * (1.5 - (n2 * f * f))
	end
	alias :isqrt :inversesqrt
	
	# Retrieve the sign of a value, or 0.
	def sign( n )
	    return 1.0 if n > 0
	    return -1.0 if n < 0
	    0
	end
	alias :sign_of :sign

	# Compute pythagorean distance between two points.
    def distance( x1, y1, x2, y2 )
        sqrt( pow( (x1 - x2), 2 ) + pow( (y1 - y2), 2 ) )
    end
    alias :distance2d :distance

    # Compute the pythagorean distance between two three dimensional points.
    def distance3d( x1, y1, x2, y2, z1, z2 )
        sqrt( pow( (x1 - x2), 2 ) + pow( (y1 - y2), 2 ) + pow( (z1 - z2), 2 ) )
    end
    
    # Compute the sum of values.
    def sum( *values )
        values.inject( :+ )
    end
    alias :sigma :sum
    
    # Return true if any input value is truthy.
    def any( *vals )
        not vals.select {|v| v }.empty?
    end
    
    # Return true if and only if all input values are truthy.
    def all( *vals )
        vals.select {|v| v }.length.eql? vals.length
    end
}
