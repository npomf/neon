require 'thread_safe'

Thread.class_eval %q{

    # Name of the thread
    attr_accessor :name
}

# Mix-in format of the ThreadPool.  The underlying `@threads` instance variable will be defined as a ThreadSafe::Array
# when first it is called, provided it has not already been defined.
module ThreadPoolMixin

    # Thread pool name
    attr_accessor :name
    # Threads in pool
    attr_reader :threads
    # Flag indicating whether or not this ThreadPool should automatically clean up dead threads on insertion or
    # deletion of existing threads.
    attr_accessor :autoclean

    # Spawn a thread into this thread pool.  Returns the thread.
    def spawn( name = nil, &block )
        thread = Thread.new( &block )
        thread.name = name
        add( thread )
        thread
    end

    # Add an existing thread to this pool.
    def add( thread )
        @threads = ThreadSafe::Array.new() if @threads.nil?
        @threads << thread
        cleanup if @autoclean
        self
    end
    alias :add_thread :add
    alias :<< :add

    # Remove a thread from this pool.
    def remove( thread )
        thr = (thread.is_a? Fixnum) ? @threads.delete_at( thread ) : @threads.delete( thread ) unless @threads.nil?
        cleanup if @autoclean
        thr
    end
    alias :remove_thread :remove
    alias :delete :remove

    # Get a thread by its name.
    def get( name )
        @threads.select {|thread| thread.name.eql? name }[0] unless @threads.nil?
    end
    alias :get_thread :get

    # Remove all threads marked as `dead`.
    def cleanup()
        @threads.each {|thread| @threads.delete( thread ) unless thread.status } unless @threads.nil?
    end
    protected :cleanup

    # Kill all threads in the pool.  The `timeout` indicates the number of seconds to wait for the thread to #join()
    # before it it forcibly killed.
    #
    # If `clean` is truthy, all dead threads will be removed from the pool.  This behavior is automatic if the
    # autoclean flag is truthy.
    def kill( timeout: 0.01, clean: true )
        unless @threads.nil?
            @threads.each {|thread| thread.kill unless thread.join( timeout ) }
            cleanup if @autoclean or clean
        end
    end
end

# A collection of threads.
class ThreadPool
    include ThreadPoolMixin

    # Initialize.
    def initialize( name = nil, autoclean: false )
        @name = name
        @autoclean = autoclean
        @threads = ThreadSafe::Array.new
    end
end

# A queued ThreadPool which has a finite number of workers.
class ThreadQueue < ThreadPool

    # Thread limit
    attr_reader :limit
    # Process queue
    attr :queue

    # Initialize.
    def initialize( name = nil, limit, autoclean: false )
        super( name, autoclean: autoclean )
        @limit = limit
        @queue = ThreadSafe::Array.new
    end

    # As ThreadPool#spawn(), except the block will be queued if the pool has reached its limit.  The position in the
    # queue is returned if the block is queued.
    def spawn( name = nil, &block )
        if @threads.length < @limit
            super( name ){
                block.call rescue nil
                @threads.delete( Thread.current )
                unless @queue.empty?
                    id, proc = @queue.shift
                    spawn( id, &proc )
                end
            }
        else
            (@queue << [name, block]).length - 1
        end
    end

    alias :maximum_threads :limit
end
