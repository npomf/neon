require_relative 'bin_io'

# A Data Structure (or DataStruct) is an object which can be deserialized from tightly packed binary data fields.  A
# `field` is specified by its name, data type, and an optional expected length and assertion.
#
# Unless `:char` or `:uchar` is specified as a data type, all `:string` fields will be interpreted as a null-terminated
# String (ends with `\0`).
module DataStruct
    
    # Configure the implementing class.
    def self.included( mod )
        mod.class_variable_set( :@@_fields, {})
        mod.class_variable_set( :@@_endianness, nil )
        mod.class_eval %q{
        
            # Define a field on the target class, specifying the instance variable's name as a symbol (see: `attr`), its
            # binary type, an optional length (implies an array if `length > 1`), and an optional assertion.
            #
            # When the `type` is a Proc or Lambda, it will be used to produce the value of the indicated field rather
            # than the default parser.
            #
            # If `type` is instead a DataStruct, that class' ::read() method will be called with the IO stream at the
            # current position.
            #
            # When a `length` is a symbol, it refers to a method (and thus potentially a field) which was previously 
            # defined.
            #
            # When `assert` is non-nil, it will be used to assert the read value.
            def self.field( sym, type, length: 1, assert: nil )
                class_eval %Q{
                    def #{sym}(); instance_variable_get("@#{sym}"); end
                    def #{sym}=( v ); instance_variable_set("@#{sym}", v ); end
                }
                @@_fields[ sym ] = [type, length, assert]
                self
            end
            
            # Preemptively set the endianness of the stream to be read.
            def self.endian( e )
                @@_endianness = e
            end
            
            # Read the binary struct data from the specified fields into this object instance.
            def read_struct( io )
                io.endianness = @@_endianness unless @@_endianness.nil?
                @@_fields.each {|sym, (type, length, assert)|
                    # Read value
                    val = case type
                        when Proc then type.call( io )
                        when DataStruct then type.read( io )
                        else
                            io.send("read_#{type}", (length.is_a? Integer) ? length : send( length ) )
                    end
                    
                    # Perform assertion and assign
                    case assert
                        when Proc then assert.call( val )
                        else
                            raise EncodingError, "#{type} field #{sym} expected #{assert} but got #{val} " unless val.eql? assert
                    end unless assert.nil?
                    send("#{sym}=", val )
                }
                self
            end
            
            # Create a new instance, reading from the specified IO and passing the given arguments.
            #
            # Object instances are created *before* their structure data is read.
            def self.read( io, *args, **kwargs )
                new( *args ).read_struct( io )
            end
        }
    end
end
