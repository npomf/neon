ext = %q{

    # Native endian mode
    NATIVE_ENDIAN = :native
    # Little endian mode
    LITTLE_ENDIAN = :little
    # Big endian mode
    BIG_ENDIAN = :big

    # Endianness (default: native)
    attr_accessor :endianness
    @endianness = NATIVE_ENDIAN
    
    # Get system endianness.
    def self.endianness()
        (big_endian?) ? BIG_ENDIAN : LITTLE_ENDIAN
    end
    
    # Check if the system is Big Endian (greatest significant bit first).
    def self.big_endian?()
        big_endian = [1].pack('L')[0].eql? "\x00"
    end
    
    # Check if the system is Little Endian (least significant bit first).
    def self.little_endian?()
        not big_endian?
    end
    
    # Read a null (`\0`, `0x00`) -terminated string.  If `strip` is truthy, the null byte will be stripped.
    def read_string( strip = false )
        buffer = ''
        buffer << (char = read_char) until char.eql? "\0"
        buffer[ 0, strip ? -2 : -1 ]
    end
    
    # Write a null-terminated string.  The terminator will be added unless one is already present.
    def write_string( str )
        write( str + ((str[-1].eql? "\0") ? '' : "\0") )
    end
    
    # Read one or more doubles.
    def read_double( count = 1 )
        sigil = case @endianness || NATIVE_ENDIAN
            when NATIVE_ENDIAN then 'D'
            when LITTLE_ENDIAN then 'E'
            when BIG_ENDIAN then 'G'
        end
        read( 8 * count ).unpack( sigil )
    end
    
    # Write one or more doubles.
    def write_double( *value )
        sigil = case @endianness || NATIVE_ENDIAN
            when NATIVE_ENDIAN then 'D'
            when LITTLE_ENDIAN then 'E'
            when BIG_ENDIAN then 'G'
        end
        write( value.pack( sigil + '*') )
    end
    
    # Read one or more floats.
    def read_float( count = 1 )
        sigil = case @endianness || NATIVE_ENDIAN
            when NATIVE_ENDIAN then 'f'
            when LITTLE_ENDIAN then 'e'
            when BIG_ENDIAN then 'g'
        end
        read( 4 * count).unpack( sigil )
    end
    
    # Write one or more floats.
    def write_float( *value )
        sigil = case @endianness || NATIVE_ENDIAN
            when NATIVE_ENDIAN then 'f'
            when LITTLE_ENDIAN then 'e'
            when BIG_ENDIAN then 'g'
        end
        write( value.pack( sigil + '*') )
    end
    
    # Read one or more longs.
    def read_long( count = 1 )
        process( 8 * count, 'q')
    end
    alias :read_int64 :read_long
    
    # Write one or more longs.
    def write_long( *value )
        unprocess( value, 'q')
    end
    alias :write_int64 :write_long
    
    # Read one or more unsigned longs.
    def read_ulong( count = 1 )
        process( 8 * count, 'Q')
    end
    alias :read_uint64 :read_ulong
    alias :read_unsigned_long :read_ulong
    
    # Write one or more unsigned longs.
    def write_ulong( *value )
        unprocess( value, 'Q')
    end
    alias :write_uint64 :write_ulong
    alias :write_unsigned_long :write_ulong
    
    # Read one or more ints.
    def read_int( count = 1 )
        process( 4 * count, 'l')
    end
    alias :read_int32 :read_int
    
    # Write one or more longs.
    def write_int( *value )
        unprocess( value, 'l')
    end
    alias :write_int32 :write_int
    
    # Read one or more unsinged ints.
    def read_uint( count = 1 )
        process( 4 * count, 'L')
    end
    alias :read_uint32 :read_uint
    alias :read_unsigned_int :read_uint
    
    # Write one or more unsigned ints.
    def write_uint( *value )
        unprocess( value, 'L')
    end
    alias :write_uint32 :write_uint
    alias :write_unsigned_int :write_uint
    
    # Read one or more shorts.
    def read_short( count = 1 )
        process( 2 * count, 's')
    end
    alias :read_int16 :read_short
    
    # Write one or more shorts.
    def write_short( *value )
        unprocess( value, 's')
    end
    alias :write_int16 :write_short
    
    # Read one or more unsigned shorts.
    def read_ushort( count = 1 )
        process( 2 * count, 'S')
    end
    alias :read_uint16 :read_ushort
    alias :read_unsigned_short :read_ushort
    
    # Write one or more unsigned shorts.
    def write_ushort( *value )
        unprocess( value, 'S')
    end
    alias :write_uint16 :write_ushort
    alias :write_unsigned_short :write_ushort
    
    # Read one or more characters.
    def read_char( count = 1 )
        process( 1 * count, 'c')
    end
    alias :read_int8 :read_char
    alias :read_byte :read_char
    
    # Write one or more longs.
    def write_char( *value )
        unprocess( value, 'c')
    end
    alias :write_int8 :write_char
    alias :write_byte :write_char
    
    # Read one or more unsigned characters.
    def read_uchar( count = 1 )
        process( 1 * count, 'C')
    end
    alias :read_uint8 :read_uchar
    alias :read_ubyte :read_uchar
    alias :read_unsigned_char :read_uchar
    alias :read_unsigned_byte :read_uchar
    
    # Write one or more unsigned characters.
    def write_uchar( *value )
        unprocess( value, 'q')
    end
    alias :write_uint8 :write_uchar
    alias :write_ubyte :write_uchar
    alias :write_unsigned_char :write_uchar
    alias :write_unsigned_byte :write_uchar
    
    # Consume values.
    def unprocess( raw, sigil )
        write( raw.pack( pack_sigil( sigil ) ) )
    end
    protected :unprocess
    
    # Produce values.
    def process( bytes, sigil )
        out = read( bytes ).unpack( pack_sigil( sigil ) )
        out.length == 1 ? out[0] : out
    end
    protected :process
    
    # Determine a pack sigil.
    def pack_sigil( base )
        base + case @endianness || NATIVE_ENDIAN
            when NATIVE_ENDIAN then ''
            when LITTLE_ENDIAN then '<'
            when BIG_ENDING then '>'
        end
    end
    protected :pack_sigil
    
    alias :read_chars :read
}

require 'stringio'

# Modify
IO.class_eval ext
StringIO.class_eval ext
