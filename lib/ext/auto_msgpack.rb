require 'msgpack'

MessagePack.class_eval %q{

    # Unpack to canonical classname token (default: 'msg_class')
    @@create_id = 'msg_class'.freeze
    # Method symbol called when inflating an implementing class; otherwise `:new` is called
    @@method_hook = :msgpack_create.freeze

    # Get the current `create_id` token.
    def self.create_id()
        @@create_id
    end

    # Set the `create_id` token.
    def self.create_id=( token )
        @@create_id = token.freeze
    end
    
    # Get the current `method_hook` symbol.
    def self.method_hook()
        @@method_hook
    end
    
    # Set the `method_hook` symbol.
    def self.method_hook=( sym )
        @@method_hook = sym.freeze
    end

    # Emulate JSON's `load()` method.
    def self.load( msg )
        msg = msg.read if msg.is_a? IO
        process( MessagePack.unpack( msg ) )
    end
    
    # Process an object.
    def self.process( obj )
        if obj.is_a? Hash
            return inflate( obj ) if obj.key? @@create_id
            obj.each {|k, v| obj[k] = process( v ) }
        else
            obj
        end
    end
    private_class_method :process
    
    # Inflate an object.
    def self.inflate( obj )
        clazz = obj[ @@create_id ]
        if Kernel.const_defined? clazz
            impl = Kernel.const_get( clazz )
            payload = obj.select {|k, v| not k.eql? @@create_id }
            impl.send( (impl.respond_to? @@method_hook) ? @@method_hook : :new, payload )
        end
    end
    private_class_method :inflate
    
    # Adds more parity to JSON
    alias :parse :unpack
}
