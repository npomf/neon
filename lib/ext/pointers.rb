require 'fiddle'

String.class_eval %q{

    # Convert to a Fiddle::Pointer.
    def pointer()
        Fiddle::Pointer.to_ptr( self )
    end
#   alias :to_ptr :pointer
}

Fixnum.class_eval %q{

    # Convert to a Fiddle::Pointer.
    def pointer()
        [ self ].pack('l').pointer
    end
#   alias :to_ptr :pointer
}

Bignum.class_eval %q{

    # Convert to a Fiddle::Pointer.
    def pointer()
        [ self ].pack('q').pointer
    end
#   alias :to_ptr :pointer
}

Float.class_eval %q{

    # Convert to a Fiddle::Pointer.
    def pointer()
        [ self ].pack('f').pointer
    end
#   alias :to_ptr :pointer
}

Array.class_eval %q{

    # Convert to a Fiddle::Pointer.
    def pointer( type = nil )
        sigil = case type || self[0]
            when Bignum, :long, :int64, :int64_t then 'q'
            when :ulong, :uint64, :uint64_t then 'Q'
            when Fixnum, :int, :int32, :int32_t then 'l'
            when :uint, :uint32, :uint32_t then 'L'
            when :short, :int16, :int16_t then 's'
            when :ushort, :uint16, :uint16_t then 'S'
            when :byte, :char then 'c'
            when :ubyte, :uchar then 'C'
            when Float, :float then 'f'
            when :double then 'd'
            when String then return join.pointer
            else
                raise Exception, "unrecognized or invalid data type: #{(type || self[0]).class.name}"
        end
        pack("#{sigil}*").pointer
    end
#   alias :to_ptr :pointer
}

Fiddle::Pointer.class_eval %q{

    # Read a Fiddle::Pointer into an array.  If only a single value exists, that value alone is returned.
    def read( type )
        sigil = case type
            when :long, :int64, :int64_t then 'q'
            when :ulong, :uint64, :uint64_t then 'Q'
            when :int, :int32, :int32_t then 'l'
            when :uint, :uint32, :uint32_t then 'L'
            when :short, :int16, :int16_t then 's'
            when :ushort, :uint16, :uint16_t then 'S'
            when :byte, :char then 'c'
            when :ubyte, :uchar then 'C'
            when :float then 'f'
            when :double then 'd'
            else
                raise Exception, "unrecognized or invalid data type: #{(type || self[0]).class.name}"
        end
        out = to_s( size ).unpack("#{sigil}*")
        out.length == 1 ? out[0] : out
    end
}

Vector.class_eval %q{

    # Convert to a Fiddle::Pointer.
    def pointer( type = nil )
        to_a.pointer( type )
    end
#   alias :to_ptr :pointer
} if Kernel.const_defined? :Vector

Matrix.class_eval %q{

    # Convert to a Fiddle::Pointer.
    def pointer( type = nil )
        to_a.flatten.pointer( type )
    end
#   alias :to_ptr :pointer
} if Kernel.const_defined? :Matrix
