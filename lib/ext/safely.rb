Kernel.class_eval %q{

    # Safely perform the block with built in error handling and reporting.
    def safely( *args, **kwargs, &block )
        begin
            block.call( *args, **kwargs )
        rescue Exception => err
            puts "#{err.class.name}: #{err}"
            puts ((err.respond_to? :backtrace) ? err.backtrace : caller).map {|line| '  ' + line }.join("\n")
        end
    end

    # Perform an assertion, raising a RuntimeError unless the operation succeeds.
    def assert( a, b = true, failure_msg = nil, is: :eql? )
        return true if a.send( is, b )
        dot = is.to_s =~ /^[a-z_]/
        raise failure_msg || "Expected #{b.inspect} but got #{a.inspect}! (Operation: #{a}#{dot ? '.' : ' '}#{is} #{b})"
    end
}
