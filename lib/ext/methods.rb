# Extension code
ext = %q{

    # Perform keyword and argument unboxing to place them into the expected order of the method's signature.  This will
    # return an array, with the 0th index the sub-array of arguments, and the 1st index the Hash of keywords.
    def unbox( *args, **kwargs )
        local_args, local_kwargs = [], {}
        parameters.each {|(type, sym)|
            case type
            when :req, :opt then local_args << obj[ sym.to_s ]
            when :keyopt, :key then local_kwargs[ sym ] = obj[ sym.to_s ]
            end
        }
        return local_args, local_kwargs
    end

    # As #unbox(), but also #call() the method.  This overrides the default syntactic sugar behavior of the #[] method,
    # allowing this alternative to be more convenient.
    def []( *args, **kwargs )
        args, kwargs = unbox( args, kwargs )
        call( *args, **kwargs )
    end
}

# Extend
Method.class_eval ext
Proc.class_eval ext
