require 'fiddle'
require 'fiddle/import'
require_relative '../ext/pointers'

module ALC
    extend Fiddle::Importer

    ALC_FUNCTIONS_MAP = {}

    def self.load_lib( lib = nil, path = nil )
        lib, path = case RbConfig::CONFIG['host_os']
            when /mac\s?os|darwin/ix
                ['libAL.dylib', '/System/Library/Frameworks/OpenAL.framework/']
            when /mswin|msys|mingw|cygwin|bcwin|wince|emc/ix
                if File.exists? 'C:/Windows/System32/soft_oal.dll'
                    ['soft_oal.dll', 'C:/Windows/System32']
                elsif File.exists? 'bin/soft_oal.dll'
                    puts 'No system OpenAL Soft, but found local DLL; using that instead' if $DEBUG
                    ['soft_oal.dll', File.expand_path('bin') ]
                else
                    warn 'No OpenAL Soft detected; falling back to default implementation'
                    ['openal32.dll', 'C:/Windows/System32']
                end
            when /linux|unix|bsd/ix
                ['libopenal.so', nil ]
        end unless lib

        dlload( path ? path + '/' + lib : lib )
        import_symbols
    end

    def self.extern( signature, *opts )
        sym, ctype, argtype = parse_signature( signature, @type_alias )
        opt = parse_bind_options( opts )
        fptr = import_function( sym, ctype, argtype, opt[ :call_type ])
        name = sym.gsub( /@.+/, '')

        # get invocation location
        file, line = caller.first.scan( /^(.+?):(\d+)/ ).map {|(f, l)| [f, l ? l.to_i - 2 : nil] }.flatten

        # register function
        ALC_FUNCTIONS_MAP[ name.to_sym ] = fptr
        module_eval( %Q{
            def #{name}( *args, &block )
                ALC_FUNCTIONS_MAP[ :#{name} ].call( *args, &block )
            end
        }, file || __FILE__, line || __LINE__ + 3 )
        module_function( name )

        # return pointer
        fptr
    end

    ALC_FALSE                                = 0
    ALC_TRUE                                 = 1
    ALC_NO_ERROR                             = 0
    ALC_EXT_CAPTURE                          = 1
    ALC_ENUMERATE_ALL_EXT                    = 1
    ALC_FREQUENCY                            = 0x1007
    ALC_REFRESH                              = 0x1008
    ALC_SYNC                                 = 0x1009
    ALC_MONO_SOURCES                         = 0x1010
    ALC_STEREO_SOURCES                       = 0x1011
    ALC_INVALID_DEVICE                       = 0xA001
    ALC_INVALID_CONTEXT                      = 0xA002
    ALC_INVALID_ENUM                         = 0xA003
    ALC_INVALID_VALUE                        = 0xA004
    ALC_OUT_OF_MEMORY                        = 0xA005
    ALC_MAJOR_VERSION                        = 0x1000
    ALC_MINOR_VERSION                        = 0x1001
    ALC_ATTRIBUTES_SIZE                      = 0x1002
    ALC_ALL_ATTRIBUTES                       = 0x1003
    ALC_DEFAULT_DEVICE_SPECIFIER             = 0x1004
    ALC_DEVICE_SPECIFIER                     = 0x1005
    ALC_EXTENSIONS                           = 0x1006
    ALC_CAPTURE_DEVICE_SPECIFIER             = 0x0310
    ALC_CAPTURE_DEFAULT_DEVICE_SPECIFIER     = 0x0311
    ALC_CAPTURE_SAMPLES                      = 0x0312
    ALC_DEFAULT_ALL_DEVICES_SPECIFIER        = 0x1012
    ALC_ALL_DEVICES_SPECIFIER                = 0x1013

    # Load symbols in accordance with: https://github.com/kcat/openal-soft/blob/master/include/AL/alc.h
    def self.import_symbols()
        typealias 'ALCboolean',     'char'
        typealias 'ALCchar',        'char'
        typealias 'const ALCchar',  'const char*'
        typealias 'ALCbyte',        'char'
        typealias 'ALCubyte',       'unsigned byte'
        typealias 'ALCshort',       'short'
        typealias 'ALCushort',      'unsigned short'
        typealias 'ALCint',         'int'
        typealias 'ALCuint',        'unsigned int'
        typealias 'const ALCuint',  'const unsigned int*'
        typealias 'ALCsizei',       'int'
        typealias 'ALCenum',        'int'
        typealias 'ALCfloat',       'float'
        typealias 'const ALCfloat', 'const float*'
        typealias 'ALCdouble',      'double'
        typealias 'ALCvoid',        'void'

        extern '* alcOpenDevice( const ALCchar )'       # return pointer to ALCDevice
        extern '* alcCreateContext( *, const ALCint* )' # return pointer to ALCcontext
        extern 'ALCboolean alcMakeContextCurrent( * )'  # param is pointer to ALCcontext
        extern 'void alcProcessContext( * )'            # " "
        extern 'void alcSuspendContext( * )'            # " "
        extern 'void alcDestroyContext( * )'            # " "
        extern '* alcGetCurrentContext( void )'         # return pointer to ALCcontext
        extern '* alcGetContextsDevice( * )'            # param is pointer to ALCcontext; return pointer to ALCdevice
        extern 'ALCenum alcGetError( void )'
        extern 'ALCboolean alcIsExtensionPresent( *, const ALCchar )'   # 1st param is pointer to ALCdevice
        extern 'void* alcGetProcAddress( *, const ALCchar )'            # " "
        extern 'ALCenum alcGetEnumValue( *, const ALCchar )'            # " "
        extern 'ALCchar* alcGetString( *, ALCenum )'                    # " "
        extern 'void alcGetIntegerv( *, ALCenum, ALCsizei, ALCint )'    # " "
        extern '* alcCaptureOpenDevice( const ALCchar, ALCuint, ALCenum, ALCsizei )'    # return pointer to ALCdevice
        extern 'ALCboolean alcCloseDevice( * )'         # param is pointer to ALCdevice
        extern 'ALCboolean alcCaptureCloseDevice( * )'  # param is pointer to ALCdevice
        extern 'void alcCaptureStart( * )'              # " "
        extern 'void alcCaptureStop( * )'               # " "
        extern 'void alcCaptureSamples( *, ALCvoid, ALCsizei )'     # 1st param is pointer to ALCdevice
        true
    end
end
