require_relative '../ext/arithmetic'
require_relative '../ext/vectors'
require_relative '../ext/pointers'

# OpenAL-enabled sound object.  Each sound represents a single sound source with individual properties which tell
# the audio renderer how it should be managed.  All sounds use a front and back buffer to permit continuous
# streaming of data; as a buffer empties, it will be refilled with the next available block of data from the
# attached decoder.
#
# Decoders should provide a String of PCM data (which can thus be converted into a `:byte` Array), which is to be
# passed to the audio renderer as the media for its associated Sound.
class Sound

    # Number of buffers to use per sound object
    BUFFERS_PER_SOURCE = 2

    # Reference distance used to determine minimum attenuation (loudest)
    @@reference_distance = 1.0
    # Determines maximum attenuation (quietest)
    @@max_distance = 1e4
    
    # Underlying stream decoder
    attr_reader :decoder
    # Pause state flag
    attr :paused
    # Mute state flag
    attr_writer :muted
    # Location vector
    attr_accessor :location
    # Orientation vector
    attr_accessor :orientation
    # Velocity vector
    attr_accessor :velocity
    # Rolloff
    attr_accessor :rolloff
    # Gain
    attr_accessor :gain
    # Pitch modulation
    attr_accessor :pitch
    # Cone angle, in degrees
    attr_accessor :cone
    # Underlying OpenAL buffers
    attr :buffers
    # Underlying OpenAL source
    attr :source
    
    # Initialize with a media decoder.
    def initialize( decoder, gain: 0.0, rolloff: 1.0, pitch: 0.0, cone: 360.0, location: Vector[0.0, 0.0, 0.0, 1.0],
                    rotation: Vector[0.0, 0.0, 0.0, 0.0], velocity: Vector[0.0, 0.0, 0.0, 0.0] )
        @decoder = decoder
        @gain = gain
        @rolloff = rolloff
        @pitch = pitch
        @cone = cone
        @location = location
        @rotation = rotation
        @velocity = velocity
        @source = nil
        @buffers = nil
    end
    
    # Stream the next block of data.
    def stream()
        update
        buffers = [0].pointer
        alGetSourcei( @source, AL_BUFFERS_PROCESSED, buffers )
        buffers.read( :int ).times {
            bfrptr = [0].pointer
            alSourceUnqueueBuffers( @source, 1, bfrptr )

            # Read and re-enqueue with next block; abort if we run out of data!
            return false unless block = @decoder.next() # Should return a string of PCM data
            alBufferData( 
                bfrptr.read( :int ), 
                (@decoder.channels.eql? 1) ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16,
                block.bytes.pointer,
                block.len,
                @decoder.sample_rate
            )
            alSourceQueueBuffers( @source, 1, bfrptr )
        }
        true    # still has data
    end
    protected :stream

    # Update OpenAL properties.
    def update()
        alSourcefv( snd.source, AL_POSITION, @location.xyz.pointer( :float ) )
        alSourcefv( snd.source, AL_VELOCITY, @velocity.xyz.pointer( :float ) )
        alSourcefv( snd.source, AL_DIRECTION, @orientation.xyz.pointer( :float ) )
        alSourcef( snd.source, AL_CONE_INNER_ANGLE, @inner_angle )
        alSourcef( snd.source, AL_CONE_OUTER_ANGLE, outer_angle )
        alSourcef( snd.source, AL_PITCH, @pitch )
        alSourcef( snd.source, AL_GAIN, @gain )
        alSourcef( snd.source, AL_ROLLOFF_FACTOR, @rolloff )
        alSourcei( snd.source, AL_REFERENCE_DISTANCE, @@reference_distance )
        alSourcei( snd.source, AL_MAX_DISTANCE, @@max_distance )
    end
    
    # Prepare the sound source and its buffers.
    def prepare()
        # generate source
        srcptr = [0].pointer
        alGenSources( 1, srcptr )
        @source = srcptr.read( :uint )
        
        # build buffers
        bfrptr = [0].pointer
        alGenBuffers( BUFFERS_PER_SOURCE, bfrptr )
        @buffers = bfrptr.read( :uint )
    end
    protected :prepare

    # Close the sound and its decoder.
    def close()
        stop
        @decoder.close
    end

    # Destroy this sound.
    def destroy!()
        close
        alDeleteBuffers( BUFFERS_PER_SOURCE, @buffers.pointer )
        alDeleteSources( 1, [@source].pointer )
    end
    
    # Stop this sound.
    def stop()
        alSourceStop( @source )
    end
    
    # Play or resume this sound.
    def play()
        prepare unless @source and @buffers
        self.paused = false
    end
    alias :resume :play

    # Pause this sound.
    def pause()
        paused( true )
    end
    
    # Pause or resume.
    def paused=( flag )
        @paused = flag
        flag ? alSourcePause( @source ) : alSourcePlay( @source )
    end
    
    # Check if paused.
    def paused?()
        @paused
    end
    
    # Check if muted.
    def muted?()
        @muted
    end
    
    # Get the outer angle of the sound cone.
    def outer_angle()
        360.0 - @cone
    end
    
    # Set the outer angle of the sound cone.
    def outer_angle=( degrees )
        @cone = 360.0 - degrees
    end

    # Set the reference distance.
    def self.reference_distance=( u )
        @@reference_distance = u
    end

    # Get the reference distance.
    def self.reference_distance()
        @@reference_distance
    end

    # Set the maximum audible distance.
    def self.max_distance=( u )
        @@max_distance = u
    end

    # Get the maximum audible distance.
    def self.max_distance()
        @@max_distance
    end
    
    alias :inner_angle :cone
    alias :inner_angle= :cone=
    alias :position :location
    alias :position= :location=
    alias :direction :orientation
    alias :direction= :orientation=
end
