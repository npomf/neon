require 'fiddle'
require 'fiddle/import'
require_relative '../ext/pointers'

module AL
    extend Fiddle::Importer

    AL_FUNCTIONS_MAP = {}

    def self.load_lib( lib = nil, path = nil )
        lib, path = case RbConfig::CONFIG['host_os']
            when /mac\s?os|darwin/ix
                ['libAL.dylib', '/System/Library/Frameworks/OpenAL.framework/']
            when /mswin|msys|mingw|cygwin|bcwin|wince|emc/ix
                if File.exists? 'C:/Windows/System32/soft_oal.dll'
                    ['soft_oal.dll', 'C:/Windows/System32']
                elsif File.exists? 'bin/soft_oal.dll'
                    puts 'No system OpenAL Soft, but found local DLL; using that instead' if $DEBUG
                    ['soft_oal.dll', File.expand_path('bin') ]
                else
                    warn 'No OpenAL Soft detected; falling back to default implementation'
                    ['openal32.dll', 'C:/Windows/System32']
                end
            when /linux|unix|bsd/ix
                ['libopenal.so', nil ]
        end unless lib

        dlload( path ? path + '/' + lib : lib )
        import_symbols
    end

    def self.extern( signature, *opts )
        sym, ctype, argtype = parse_signature( signature, @type_alias )
        opt = parse_bind_options( opts )
        fptr = import_function( sym, ctype, argtype, opt[ :call_type ])
        name = sym.gsub( /@.+/, '')

        # get invocation location
        file, line = caller.first.scan( /^(.+?):(\d+)/ ).map {|(f, l)| [f, l ? l.to_i - 2 : nil] }.flatten

        # register function
        AL_FUNCTIONS_MAP[ name.to_sym ] = fptr
        module_eval( %Q{
            def #{name}( *args, &block )
                AL_FUNCTIONS_MAP[ :#{name} ].call( *args, &block )
            end
        }, file || __FILE__, line || __LINE__ + 3 )
        module_function( name )

        # return pointer
        fptr
    end

    AL_VERSION_1_0                  = 1
    AL_VERSION_1_1                  = 1

    AL_NONE                         = 0x0000
    AL_FALSE                        = 0x0000
    AL_TRUE                         = 0x0001
    AL_SOURCE_RELATIVE              = 0x0202
    AL_CONE_INNER_ANGLE             = 0x1001
    AL_CONE_OUTER_ANGLE             = 0x1002
    AL_PITCH                        = 0x1003
    AL_POSITION                     = 0x1004
    AL_DIRECTION                    = 0x1005
    AL_VELOCITY                     = 0x1006
    AL_LOOPING                      = 0x1007
    AL_BUFFER                       = 0x1009
    AL_GAIN                         = 0x100A
    AL_MIN_GAIN                     = 0x100D
    AL_MAX_GAIN                     = 0x100D
    AL_ORIENTATION                  = 0x100F
    AL_SOURCE_STATE                 = 0x1010
    AL_INITIAL                      = 0x1011
    AL_PLAYING                      = 0x1012
    AL_PAUSED                       = 0x1013
    AL_STOPPED                      = 0x1014
    AL_BUFFERS_QUEUED               = 0x1015
    AL_BUFFERS_PROCESSED            = 0x1016
    AL_SEC_OFFSET                   = 0x1024
    AL_SAMPLE_OFFSET                = 0x1025
    AL_BYTE_OFFSET                  = 0x1026
    AL_SOURCE_TYPE                  = 0x1027
    AL_STATIC                       = 0x1028
    AL_STREAMING                    = 0x1029
    AL_UNDETERMINED                 = 0x1030
    AL_FORMAT_MONO8                 = 0x1100
    AL_FORMAT_MONO16                = 0x1101
    AL_FORMAT_STEREO8               = 0x1102
    AL_FORMAT_STEREO16              = 0x1103
    AL_REFERENCE_DISTANCE           = 0x1020
    AL_ROLLOFF_FACTOR               = 0x1021
    AL_CONE_OUTER_GAIN              = 0x1022
    AL_MAX_DISTANCE                 = 0x1023
    AL_FREQUENCY                    = 0x2001
    AL_BITS                         = 0x2002
    AL_CHANNELS                     = 0x2003
    AL_SIZE                         = 0x2004
    AL_UNUSED                       = 0x2010
    AL_PENDING                      = 0x2011
    AL_PROCESSED                    = 0x2012
    AL_NO_ERROR                     = 0x0000
    AL_INVALID_NAME                 = 0xA001
    AL_INVALID_ENUM                 = 0xA002
    AL_INVALID_VALUE                = 0xA003
    AL_INVALID_OPERATION            = 0xA004
    AL_OUT_OF_MEMORY                = 0xA005
    AL_VENDOR                       = 0xB001
    AL_VERSION                      = 0xB002
    AL_RENDERER                     = 0xB003
    AL_EXTENSIONS                   = 0xB004
    AL_DOPPLER_FACTOR               = 0xC000
    AL_DOPPLER_VELOCITY             = 0xC001
    AL_SPEED_OF_SOUND               = 0xC003
    AL_DISTANCE_MODEL               = 0xD000
    AL_INVERSE_DISTANCE             = 0xD001
    AL_INVERSE_DISTANCE_CLAMPED     = 0xD002
    AL_LINEAR_DISTANCE              = 0xD003
    AL_LINEAR_DISTANCE_CLAMPED      = 0xD004
    AL_EXPONENT_DISTANCE            = 0xD005
    AL_EXPONENT_DISTANCE_CLAMPED    = 0xD006

    # Load symbols in accordance with: http://www.opensource.apple.com/source/OpenAL/OpenAL-48.7/Source/OpenAL/al/al.h
    def self.import_symbols()
        typealias 'ALboolean',      'char'
        typealias 'ALchar',         'char'
        typealias 'const ALchar',   'const char*'
        typealias 'ALbyte',         'char'
        typealias 'ALubyte',        'unsigned byte'
        typealias 'ALshort',        'short'
        typealias 'ALushort',       'unsigned short'
        typealias 'ALint',          'int'
        typealias 'ALuint',         'unsigned int'
        typealias 'const ALuint',   'const unsigned int*'
        typealias 'ALsizei',        'int'
        typealias 'ALenum',         'int'
        typealias 'ALfloat',        'float'
        typealias 'const ALfloat',  'const float*'
        typealias 'ALdouble',       'double'
        typealias 'ALvoid',         'void'

        extern 'void alEnable( ALenum )'
        extern 'void alDisable( ALenum )'
        extern 'ALboolean alIsEnabled( ALenum )'
        extern 'const *char alGetString( ALenum )'
        extern 'void alGetBooleanv( ALenum, ALboolean* )'
        extern 'void alGetIntegerv( ALenum ALint* )'
        extern 'void alGetFloatv( ALenum, ALfloat* )'
        extern 'void alGetDoublev( ALenum, ALdouble* )'
        extern 'ALboolean alGetBoolean( ALenum )'
        extern 'ALint alGetInteger( ALenum )'
        extern 'ALfloat alGetFloat( ALenum )'
        extern 'ALdouble alGetDouble( ALenum )'
        extern 'ALenum alGetError( void )'
        extern 'ALboolean alIsExtensionPresent( const ALchar* )'
        extern 'void* alGetProcAddress( const ALchar* )'
        extern 'ALenum alGetEnumValue( const ALchar* )'
        extern 'void alListenerf( ALenum, ALfloat )'
        extern 'void alListener3f( ALenum, ALfloat, ALfloat, ALfloat )'
        extern 'void alListenerfv( ALenum, const ALfloat* )'
        extern 'void alListeneri( ALenum, ALint )'
        extern 'void alListener3i( ALenum, ALint, ALint, ALint )'
        extern 'void alListeneriv( ALenum, const ALint* )'
        extern 'void alGetListenerf( ALenum, ALfloat )'
        extern 'void alGetListener3f( ALenum, ALfloat, ALfloat, ALfloat )'
        extern 'void alGetListenerfv( ALenum, const ALfloat* )'
        extern 'void alGetListeneri( ALenum, ALint )'
        extern 'void alGetListener3i( ALenum, ALint, ALint, ALint )'
        extern 'void alGetListeneriv( ALenum, const ALint* )'
        extern 'void alGenSources( ALsizei, ALuint* )'
        extern 'void alDeleteSources( ALsizei, const ALuint* )'
        extern 'ALboolean alIsSource( ALuint )'
        extern 'void alSourcef( ALenum, ALfloat )'
        extern 'void alSource3f( ALenum, ALfloat, ALfloat, ALfloat )'
        extern 'void alSourcefv( ALenum, const ALfloat* )'
        extern 'void alSourcei( ALenum, ALint )'
        extern 'void alSource3i( ALenum, ALint, ALint, ALint )'
        extern 'void alSourceiv( ALenum, const ALint* )'
        extern 'void alGetSourcef( ALenum, ALfloat )'
        extern 'void alGetSource3f( ALenum, ALfloat, ALfloat, ALfloat )'
        extern 'void alGetSourcefv( ALenum, const ALfloat* )'
        extern 'void alGetSourcei( ALenum, ALint )'
        extern 'void alGetSource3i( ALenum, ALint, ALint, ALint )'
        extern 'void alGetSourceiv( ALenum, const ALint* )'
        extern 'void alSourcePlayv( ALsizei, const ALuint* )'
        extern 'void alSourceStopv( ALsizei, const ALuint* )'
        extern 'void alSourceRewindv( ALsizei, const ALuint* )'
        extern 'void alSourcePausev( ALsizei, const ALuint* )'
        extern 'void alSourcePlay( const ALuint )'
        extern 'void alSourceStop( const ALuint )'
        extern 'void alSourceRewind( const ALuint )'
        extern 'void alSourcePause( const ALuint )'
        extern 'void alSourceQueueBuffers( ALuint, ALsizei, const ALuint* )'
        extern 'void alSourceUnqueueBuffers( ALuint, ALsizei, ALuint* )'
        extern 'void alGenBuffers( ALsizei, ALuint* )'
        extern 'void alDeleteBuffers( ALsizei, const ALuint* )'
        extern 'ALboolean alIsBuffer( ALuint )'
        extern 'void alBufferf( ALenum, ALfloat )'
        extern 'void alBuffer3f( ALenum, ALfloat, ALfloat, ALfloat )'
        extern 'void alBufferfv( ALenum, const ALfloat* )'
        extern 'void alBufferi( ALenum, ALint )'
        extern 'void alBuffer3i( ALenum, ALint, ALint, ALint )'
        extern 'void alBufferiv( ALenum, const ALint* )'
        extern 'void alGetBufferf( ALenum, ALfloat )'
        extern 'void alGetBuffer3f( ALenum, ALfloat, ALfloat, ALfloat )'
        extern 'void alGetBufferfv( ALenum, const ALfloat* )'
        extern 'void alGetBufferi( ALenum, ALint )'
        extern 'void alGetBuffer3i( ALenum, ALint, ALint, ALint )'
        extern 'void alDopplerFactor( ALfloat )'
        extern 'void alDopplerVelocity( ALfloat )'
        extern 'void alSpeedOfSound( ALfloat )'
        extern 'void alDistanceModel( ALenum )'
        true
    end

    # As `alIsExtensionPresent` but returns either `true` or `false`.
    def al_extension?( extstr )
        send( :alIsExtensionPresent, extstr ).read( :char ).eql? AL_TRUE
    end

    # As `alIsEnabled` but returns either `true` or `false`.
    def al_enabled?( enum )
        send( :alIsEnabled, enum ).read( :char ).eql? AL_TRUE
    end

    # As `alGetBoolean` but returns either `true` or `false`.
    def al_bool( enum )
        send( :alGetBoolean, enum ).read( :char ).eql? AL_TRUE
    end
    alias :al_boolean :al_bool

    # As `alGetInteger` but returns an instance of Fixnum.
    def al_int( enum )
        send( :alGetInteger, enum ).read( :int )
    end
    alias :al_integer :al_int

    # As `alGetInteger` but returns a single-precision instance of Float (<= 32 bits).
    def al_float( enum )
        send( :alGetFloat, enum ).read( :float )
    end

    # As `alGetInteger` but returns a double-precision instance of Float (<= 64 bits).
    def al_double( enum )
        send( :alGetDouble, enum ).read( :double )
    end

    # As `alIsSource` but returns either `true` or `false`.
    def al_source?( uint )
        send( :alIsSource, uint ).read( :char ).eql? AL_TRUE
    end

    # As `alIsBuffer` but returns either `true` or `false`.
    def al_buffer?( uint )
        send( :alIsBuffer, uint ).read( :char ).eql? AL_TRUE
    end
end
