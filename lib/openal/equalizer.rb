include EFX if require_relative 'efx'
require_relative '../ext/pointers'

# EFX-based equalizer.
module Equalizer
    extend self

    # Internal AL Filter pointer
    attr :filter
    # Low gain
    attr_accessor :low_gain
    # Low cutoff
    attr_accessor :low_cutoff
    # High gain
    attr_accessor :high_gain
    # High cutoff
    attr_accessor :high_cutoff
    # Mid 1 gain
    attr_accessor :mid1_gain
    # Mid 1 center
    attr_accessor :mid1_center
    # Mid 1 width
    attr_accessor :mid1_width
    # Mid 2 gain
    attr_accessor :mid2_gain
    # Mid 2 center
    attr_accessor :mid2_center
    # Mid 2 width
    attr_accessor :mid2_width

    # Set from an array of values.
    def set( low_gain, low_cutoff, mid1_gain, mid1_center, mid1_width, mid2_gain, mid2_center, mid2_width, high_gain,
             high_cutoff )
        @low_gain = low_gain
        @low_cutoff = low_cutoff
        @mid1_gain = mid1_gain
        @mid1_center = mid1_center
        @mid1_width = mid1_width
        @mid2_gain = mid2_gain
        @mid2_center = mid2_center
        @mid2_width = mid2_width
    end

    # Updates the OpenAL state of the equalizer.
    def update()
        gen_filter if @filter.nil?
        alFilterf( @filter, AL_EQUALIZER_LOW_GAIN, @low_gain )
        alFilterf( @filter, AL_EQUALIZER_LOW_CUTOFF, @low_cutoff )
        alFilterf( @filter, AL_EQUALIZER_MID1_GAIN, @mid1_gain )
        alFilterf( @filter, AL_EQUALIZER_MID1_CENTER, @mid1_center )
        alFilterf( @filter, AL_EQUALIZER_MID1_WIDTH, @mid1_width )
        alFilterf( @filter, AL_EQUALIZER_MID2_GAIN, @mid2_gain )
        alFilterf( @filter, AL_EQUALIZER_MID2_CENTER, @mid2_center )
        alFilterf( @filter, AL_EQUALIZER_MID2_WIDTH, @mid2_width )
        alFilterf( @filter, AL_EQUALIZER_HIGH_GAIN, @high_gain )
        alFilterf( @filter, AL_EQUALIZER_HIGH_CUTOFF, @high_cutoff )
    end

    # Close the equalizer, discarding its filter.
    def close()
        alDeleteFilters( 1, @filter.pointer )
        @filter = nil
    end
    alias :destroy! :close

    # Generate the underlying AL Filter.
    def gen_filter()
        ptr = 0.pointer
        alGenFilters( 1, ptr )
        @filter = ptr.read( :int )
    end
    protected :gen_filter
end

# A list of equalizer presets.
module Equalizer::Preset

    # Flat
    FLAT = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ]
end
