require_relative '../ext/arithmetic'
require_relative '../ext/vectors'
require_relative '../ext/pointers'

# Listener object.
#
# The listener is essentially a static reference, and contains only the most basic properties necessary to
# determine the locality of the listener -- that is, the representation of what "hears" the audio within the audio
# renderer.
module Listener
    extend self

    # Location vector
    attr_accessor :location
    # Orientation vector
    attr_accessor :orientation
    # Velocity vector
    attr_accessor :velocity

    # Update the listener properties.
    def update()
        alListenerfv( AL_POSITION, @location.xyz.pointer( :float ) )
        alListenerfv( AL_VELOCITY, @velocity.xyz.pointer( :float ) )
        alListenerfv( AL_ORIENTATION, @orientation.xyz.pointer( :float ) )
    end

    alias :position :location
    alias :position= :location=
    alias :direction :orientation
    alias :direction= :orientation=
end
