require_relative 'external'
require_relative 'reference'

# External reference to a Vertex Array Object (VAO).
class VertexArray < External
    include Reference

    # Attribute pointer cache
    attr_reader :pointers
    # Primitive type to draw
    attr_reader :primitive
    # Number of underlying elements in buffer
    attr_reader :elements

    # Initialize.
    def initialize( id, elements, primitive, *attrib_pointers, **attrs )
        super( id, attrs )
        @elements = elements
        @primitive = primitive
        @pointers = attrib_pointers
        register
    end

    # Draw this vertex array.  You must first bind the analogous buffer.
    def draw()
        glDrawArrays( @primitive, 0, @elements )
    end

    # Destroy this vertex array.
    def destroy!()
        glDeleteVertexArrays( [@id].pointer )
        super
    end

    # Create a new VAO in OpenGL and return its name.
    def self.vertex_array( vbo, elements, primitive = GL_TRIANGLES, *attrib_pointers )
        vbo.bind

        # generate vao
        vao = 0.pointer
        glGenVertexArrays( 1, vao )
        id = vao.read( :int )

        # bind and activate
        glBindVertexArray( id )
        attrib_pointers.map {|pointer| (pointer.is_a? AttribPointer) ? pointer : AttribPointer.new( pointer ) } # coerce inputs
        attrib_pointers.each_with_index {|p, i|
            p.index = index
            glVertexAttribPointer( i, p.components, p.type, p.normalized, p.stride, p.offset )
            glEnableVertexAttribArray( i )
        }
        id
    end

    # Create from buffer and attribute pointers.
    def self.from( vbo, elements, primitive = GL_TRIANGLES, *attrib_pointers )
        id = vertex_array( vbo, elements, primitive, *attrib_pointers )
        new( id, elements, primitive, *attrib_pointers )
    end

    alias :size :elements
    alias :count :elements

    # Attribute Pointer class.
    class AttribPointer
        include Persistent

        # Current index; set by VertexArray
        attr_accessor :index
        # Number of components per vertex (default: 4)
        attr_reader :components
        # GL type enum (default: GL_FLOAT)
        attr_reader :type
        # Indicates whether or not this attribute pointer refers to normalized data (default: false)
        attr :normalized
        # Density of data packing (default: 0)
        attr_reader :stride
        # Offset of first index
        attr_reader :offset

        # Initialize.
        def initialize( components: 4, type: GL_FLOAT, normalized: false, stride: 0, offset: 0 )
            @index = nil
            @components = components
            @type = type
            @normalized = normalized
            @stride = stide
            @offset = offset
        end

        # Return whether or not this pointer refers to normalized data as a GLenum.
        def normalized()
            case @normalized
                when true, 1, GL_TRUE then GL_TRUE
                else GL_FALSE
            end
        end

        # Get the normalization flag as a boolean value.
        def normalized?()
            normalized.eql? GL_TRUE
        end

        # Serialize.
        def dump()
            super.merge({
                components: @components,
                type: @type,
                normalized: @normalized,
                stride: @stride,
                offset: @offset
            })
        end
        protected :dump

        # Reconstitute.
        def self.msg_create( obj )
            new( obj )
        end
    end
end

# Alias for VertexArray
VAO = VertexArray
