require 'chunky_png'
require_relative 'external'
require_relative 'reference'

# External reference to a Texure object; caches its texture parameters as attributes.
class Texture < External
    include Reference

    # Texture type
    attr_reader :type

    # Initialize.
    def initialize( id, type, **parameters )
        super( id, parameters )
        @type = type
        register
    end
    
    # Bind this texture.
    def bind( unit = GL_TEXTURE0 )
        glActiveTexture( unit )
        glBindTexture( @type, @id )
        super
    end
    
    # Unbind this texture.
    def unbind( unit = GL_TEXTURE0 )
        glActiveTexture( unit )
        glBindTexture( @type, 0 )
        super
    end
    
    # Update this texture's parameters.
    def update()
        @attrs.each {|enum, value|
            enum = const_get( enum ) unless enum.is_a? Fixnum
            begin
                case value
                    when Array, Vector
                        f = value[0].is_a? Float
                        target = f ? :glTextureParameterfv : :glTextureParameteriv
                        send( target, @id, enum, value.pointer( f ? :float : :int ) )
                    when Float
                        glTextureParameterf( @id, enum, value )
                    when Integer, Fixnum
                        glTextureParameteri( @id, enum, value )
                end
            rescue
                warn "Unrecognized texture parameter 0x#{enum.to_s( 16 )}"
            end
        }
    end
    
    # Destroy this texture.
    def destroy!()
        glDeleteTextures( [@id].pointer )
        super
    end
    
    # Read a PNG image and return it.  A binary-encoded String will be read as an IO stream.
    def self.read_image( source )
        case source
            when File then ChunkyPNG::Image.from_file( source )
            when IO then ChunkyPNG::Image.from_datastream( ChunkPNG::Datastream.from_io( source ) )
            when String
                case source.encoding
                    when Encoding::BINARY then ChunkyPNG::Image.from_datastream( ChunkPNG::Datastream.from_blob( source ) )
                    else ChunkyPNG::Image.from_datastream( ChunkPNG::Datastream.from_io( source ) )
                end
        end
    end
    
    # Create a new texture in OpenGL and return its name.
    def self.texture( width, height, pixels, type: GL_TEXTURE_2D, mipmap: 0, depth: 0 )
        texptr = 0.pointer
        glGenTextures( 1, texptr )
        id = texptr.read( :int )

        # Massage data as needed
        pixels = pixels.bytes if pixels.is_a? String
        pixels = pixels.pointer( :int ) if pixels.is_a? Array

        # Bind and fill
        glBindTexture( type, id )
        case type
            when GL_TEXTURE_1D, 
                 GL_PROXY_TEXTURE_1D
                glTexImage1D( id, mipmap, GL_RGBA, width, 0, GL_RGBA, GL_INT, pixels )
            when GL_TEXTURE_3D, 
                 GL_PROXY_TEXTURE_3D, 
                 GL_TEXTURE_2D_ARRAY, 
                 GL_PROXY_TEXTURE_2D_ARRAY
                glTexImage3D( id, mipmap, GL_RGBA, width, height, depth, 0, GL_RGBA, GL_INT, pixels )
            when GL_TEXTURE_2D,
                 GL_PROXY_TEXTURE_2D,
                 GL_TEXTURE_1D_ARRAY,
                 GL_PROXY_TEXTURE_1D_ARRAY,
                 GL_TEXTURE_RECTANGLE,
                 GL_PROXY_TEXTURE_RECTANGLE,
                 GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                 GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                 GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                 GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                 GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                 GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
                 GL_PROXY_TEXTURE_CUBE_MAP
                glTexImage2D( id, mipamp, GL_RGBA, width, height, 0, GL_RGBA, GL_INT, pixels )
        end
        id
    end
    
    # Load from a File, String buffer, or IO.
    #
    # MessagePacked data files **must** declare their `read_mode` property as `"buffer"` to properly encapsulate the
    # underlying data stream *if and only if* the image data has been packed into the file.
    #
    # If instead the data file refers to an external image, using `"file"` or discarding the `read_mode` property should
    # be used to indicate that the source target is external.
    def self.from( file, type: GL_TEXTURE_2D, mipmap: 0, depth: 0, **parameters )
        image = read_image( file )
        pixels = image.pixels.map {|p| p.rgba }
        id = texture( image.width, image.height, pixels, type: type, mipmap: mipmap, depth: depth )
        tex = new( id, parameters )
        tex.update
        tex
    end
end
