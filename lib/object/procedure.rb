require_relative '../ext/safely'
require_relative 'resource'

# A procedure is a wrapper to the builtin Proc class, providing Resource-like functionality.
class Procedure < Proc
    include ResourceMixin

    # Initialize.  Allows the definition of attributes via its keyword arguments.
    def initialize( id = Resource.uuid, **attrs, &block )
        super( &block )
        @attrs = attrs
        @id = id
    end

    # Safe #call() variant, which will attempt to automatically handle exceptions.
    def safe_call( *args )
        safely { call( *args ) }
    end
end
