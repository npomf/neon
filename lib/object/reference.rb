require 'thread_safe'

# Denotes an object which refers to another, such as a name in a C library.  Multiple instances of these resources can 
# be tracked.
#
# `@@current` is the active instance, whilst `@@instances` is the cache of all known instances.
module Reference

    # "Upgrade" the including module or class with the appropriate functionality.
    def self.included( mod )
        mod.class_variable_set( :@@previous, 0 )
        mod.class_variable_set( :@@current, 0 )
        mod.class_variable_set( :@@instances, ThreadSafe::Cache.new )
        mod.class_eval %q{
            # Register this object.
            def register()
                @@instances[ @id ] = self
            end
            protected :register
        
            # Swap to this instance, marking any previous binding as the one to be restored (see: ::restore).  Once the
            # previous instance is marked, this method calls bind().
            def swap()
                @@previous = @@current.clone
                bind
            end
        
            # Use this reference.
            def bind()
                @@current = @id
            end
            
            # Stop using this program
            def unbind()
                @@current = 0
            end
        
            # Destroy this reference.
            def destroy!()
                @@instances.delete( @id )
            end
            
            # Restore the previous binding.
            def self.restore()
                @@instances[ @@previous ].bind
            end
            
            # Get the current reference ID.
            def self.current()
                @@current
            end
            
            # Get the active (in-use) reference object.
            def self.active()
                @@instances[ @@current ]
            end
            
            # Get an extant instance by its ID.
            def self.instance( id )
                (id.is_a? Fixnum) ? @@instances[ id ] : @@instances.each {|id, obj| return obj if obj.name.eql? id }
            end
        }
    end
end
