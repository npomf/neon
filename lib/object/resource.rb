require 'thread_safe'

# The core of a Resource, which allows it to store an ID and have attributes.
module ResourceMixin

    # Identifier
    attr_reader :id
    # Inner attributes
    attr :attrs

    # Get an attribute.
    def []( key )
        @attrs[ key ]
    end
    alias :get :[]

    # Set an attribute.
    def []=( key, value )
        @attrs[ key ] = value
    end
    alias :set :[]=
end

# Basic object resource.  Provides an ID and a means to contain arbitrary attributes.
class Resource
    include ResourceMixin

    # Initialize.
    def initialize( id = Resource.uuid(), **attrs )
        @id = id
        @attrs = ThreadSafe::Hash.new.merge! attrs
    end

    # Create a V4-like psuedo-random UUID.
    def self.uuid()
        "%08x-%04x-%04x-%04x-%04x%08x" % Array.new( 16 ){ rand( 256 ).chr }.join.unpack('NnnnnN')
    end
end
