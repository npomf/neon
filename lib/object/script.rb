require 'monitor'
require_relative 'procedure'
require_relative '../core'
require_relative '../ext/methods'

# Simple container object for a script.  Scripts are compiled from source or a Proc, and can be executed either directly
# or as threads.  By default, their nature is as follows:
#
# * Compile in a sandbox evaluator
# * Run
class Script < Procedure
    include MonitorMixin

    # Underlying thread
    attr :thread
    # Thread tracking condition
    attr :condition

    # Create a new script.  The `id` is its name.  If the `source` is nil, the `block` must be provided.
    def initialize( id, source = nil, &block )
        block = @@compiler.call( source ) if source and not block
        super( id, &block )
        @thread = nil
        @condition = new_cond
    end

    # Invoke the script in a Thread, returning the newly created Thread.  The thread will be added to the core thread
    # pool.
    #
    # Multiple "instances" of a Script can be created by successive invocations of this method.
    def run( *args, **kwargs )
        thr = Thread.new { self[ *args, **kwargs ] }
        Neon << thr
        thr
    end

    # As #run(), except the thread instance is tracked internally.  Successive invocations of this method **will**
    # block until the underlying thread terminates.
    def await( *args, **kwargs )
        synchronize {
            @condition.wait_while { not @thread.nil? or @thread.status }
            @thread = Thread.new { self[ *args, **kwargs ] }
            Neon << thr
            @thread
        }
    end

    # Attempts to send method invocations to the underlying thread if it has been populated.
    def method_missing( sym, *args, **kwargs, &block )
        @thread.send( sym, *args, **kwargs, &block ) if not @thread.nil? and @thread.respond_to? sym
        super
    end

    # Set the compiler.
    def self.compiler=( o )
        @@compiler = o
    end

    # Get the compiler.
    def self.compiler()
        @@compiler
    end

    # Compile the script's source into a Proc.
    def compile()
        eval("proc { #{@source} }")
    end
    protected :compile

    # Load a script from a file or IO stream.  The script's ID will be the name of the file stripped of its extension.
    def self.load( io )
        name = case io
            when File, String then File.basename( io ).gsub( File.extname( io ), '')
            else 'Anonymous Script'
        end
        new( name, ((io.is_a? IO) ? io : open( io )).read )
    end

    # Compiler for scripts; default is the engine namespace
    @@compiler = &:compile
end
