require_relative 'external'
require_relative 'reference'

# External reference to a GLSL Shader Program.  Its attributes are uniforms.  Caches the IDs of attached shaders.
class Program < External
    include Reference
    
    # Shaders attached to this program
    attr_reader :shaders
    
    # Initialize.
    def initialize( id, *shaders, **uniforms )
        super( id, uniforms )
        @shaders = shaders
        register
    end
    
    # Set a uniform.  Once set defined, the uniform will be updated in GL.
    def []=( location = 0, uniform, value )
        super
        location = glGetUniformLocation( @id, uniform.to_s ) if location.eql? 0
        case value
            when Float
                glProgramUniform1f( @id, location, value )
            when Fixnum, Integer
                glProgramUniform1i( @id, location, value )
            when Array, Vector
                len = value.length
                f = value[0].is_a? Float
                send("glProgramUniform#{len}#{f ? 'f' : 'i'}", @id, location, value.pointer( f ? :float : :int ) )
            when Matrix then
                r, c = value.row_size, value.col_size
                dim = r == c ? r : "#{c}x#{r}"
                f = value[0, 0].is_a? Float
                send("glProgramUniformMatrix#{dim}#{f ? 'f' : 'i'}v",@id, location, value.pointer( f ? :double : :int ) )
        end if loc > 0
    end
    
    # Update all uniforms on this program.
    def update()
        @attrs.each {|uniform, value| Neon.set_uniform( @id, uniform, value ) }
    end
    
    # Link or re-link this program.
    def link()
        glLinkProgram( @id )
    end
    
    # Check if the program was linked.
    def linked?()
        ptr = 0
        glGetProgramiv( @id, GL_LINK_STATUS, ptr )
        ptr.read( :int ).eql? GL_TRUE
    end
    
    # Use this program.
    def bind()
        glUseProgram( @id )
        super
    end
    alias :use :bind
    
    # Stop using this program
    def unbind()
        glUseProgram( 0 )
        super
    end
    alias :remove :unbind
    
    # Destroy this program.
    def destroy!()
        glDeleteProgram( @id )
        super
    end
    
    # Create a shader program in OpenGL and return its name.
    def self.program( *shader_ids )
        id = glCreateProgram()
        shader_ids.each {|shader| glAttachShader( shader ) }
        glLinkProgram( id )
        id
    end
    
    # Create from a list of shaders and map of uniform names and values.
    def self.from( *shaders, **uniforms )
        shader_ids = shaders.map {|shader| 
            case shader
                when Fixnum then shader
                when Shader then shader.id
                when Hash then Neon.build_shader( shader['source'], shader['type'] )
                else raise ArgumentException, "Illegal shader type '#{shader.class.name}'; expected Shader, Fixnum, or Hash!"
            end
        }
        id = program( *shader_ids )
        new( id, *shader_ids, uniforms )
    end
end
