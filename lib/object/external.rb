require_relative 'resource'
require_relative '../ext/methods'
require_relative '../ext/auto_msgpack'

# Refers to an object which itself refers to an external resource or value, such as those created by OpenGL and OpenAL.
# The identifier of an External object is always the uint enum name of the referent object.
class External < Resource

    # Human-readable name
    attr_accessor :name

    # Initialize.
    def initialize( id = nil, name: nil, **attrs )
        super( id, attrs )
        @name = name
    end

    # Reinitialization from serialized format.  This will automatically unbox the parameters into their correct order
    # and format, with support for both traditional and keyword arguments.
    def self.msg_create( obj )
        from( obj )
    end
end
