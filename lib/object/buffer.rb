require 'fiddle'
require_relative 'external'
require_relative 'reference'

# External reference to a Vertex Buffer Object (VBO).
class Buffer < External
    include Reference

    # Buffer type
    attr_reader :type
    # Buffer usage
    attr_reader :usage
    
    # Initialize.
    def initialize( id, type, usage )
        super( id )
        @type = type
        @usage = usage
        register
    end
    
    # Bind this buffer.
    def bind()
        glBindBuffer( @type, @id )
        super
    end
    
    # Unbind this buffer.
    def unbind()
        glBindBuffer( @type, 0 )
        super
    end

    # Destroy this buffer.
    def destroy!()
        glDeleteBuffers( [@id].pointer )
        super
    end
    
    # Create a new VBO in OpenGL and return its name.
    def self.buffer( data, type: GL_ARRAY_BUFFER, usage: GL_STATIC_DRAW )
        vbo = 0.pointer
        glGenBuffers( 1, vbo )
        id = vbo.read( :int )

        # coerce array
        buffer = (data.is_a? Fiddle::Pointer) ? data : data.pointer

        # bind and populate
        glBindBuffer( type, id )
        glBufferData( type, buffer.size, buffer, usage )
        id
    end
    
    # Create from an array.
    def self.from( data, type: GL_ARRAY_BUFFER, usage: GL_STATIC_DRAW )
        id = buffer( data, type: type, usage: usage )
        new( id, type, usage )
    end
end

# Alias for Buffer
VBO = Buffer
