require 'thread_safe'
require_relative 'external'

# External reference to a GLSL Shader.
class Shader < External
    
    # List of all extant shaders
    @@extant = ThreadSafe::Cache.new
    
    # Shader type
    attr_reader :type
    
    # Initialize.
    def initialize( id, type )
        super( id )
        @type = type
        register
    end
    
    # Set the shader source.  You will need to recompile the shader with `compile()` afterwards.
    def source=( glsl )
        lines = glsl.each_line.to_a
        glShaderSource(
            id,
            lines.length,
            glsl.pointer.ref,
            lines.map {|line| line.length }.pointer( :int ),
            0
        )
    end
    
    # Compile or recompile this shader.
    def compile()
        glCompileShader( @id )
    end
    
    # Check if the shader was compiled.
    def compiled?()
        ptr = false
        glGetShaderiv( @id, GL_COMPILE_STATUS, ptr )
        ptr.read( :int ).eql? GL_TRUE
    end
    
    # Destroy this shader.
    def destroy!()
        glDeleteShader( @id )
        @@extant.delete( @id )
    end
    
    # Get an existing Shader object by its ID.
    def self.instance( id )
        @@extant[ id ]
    end
    
    # Create a new shader in OpenGL and return its name.
    def self.shader( source, type )
        lines = ((source.is_a? IO) ? source.read : source).each_line.to_a
        id = glCreateShader( type )
        glShaderSource(
            id,
            lines.length,
            source.pointer.ref,
            lines.map {|line| line.length }.pointer( :int ),
            0
        )
        glCompileShader( id )
        id
    end
    
    # Create from GLSL code or an IO/File object.  Type must be one of:
    # * A compute shader: `GL_COMPUTE_SHADER` (GL >= 4.3)
    # * A vertex shader:  `GL_VERTEX_SHADER`
    # * A tesselation control shader:    `GL_TESS_CONTROL_SHADER`
    # * A tesselation evaluation shader: `GL_TESS_EVALUATION_SHADER`
    # * A geometry shader:    `GL_GEOMETRY_SHADER`
    # * Or a fragment shader: `GL_FRAGMENT_SHADER`
    def self.from( source, type )
        id = shader( source, type )
        new( id, type )
    end
end
