require 'thread_safe'
require_relative '../core'

# Abstract input wrapper.
class Input
    
    # Input bindings
    attr_reader :bindings
    
    # Configure all bindings.
    def initialize( bindings = {} )
        @bindings = ThreadSafe::Hash.new()
        @bindings.merge! bindings.map {|k, v| wrap( v ) }
    end
    
    # Set a binding.  A block may be specified in place of a target function, Proc, Method, or evaluable String.
    def bind( target, func = nil, &block )
        @bindings[ target ] = wrap( func || block )
    end
    
    # Remove a binding.
    def unbind( target )
        @bindings.delete target if @bindings
    end

    # Fire a binding.  Does nothing unless the target exists.
    def fire( target, *args )
        @bindings[ target ].call( *args ) if @bindings.key? target
    end

    # Wrap up a binding into a usable object.
    def wrap( obj )
        case obj
            when Proc, Method then obj
            when String then proc { Neon.eval( obj ) }
            when Symbol then method( obj )
        #   when Fiddle::Function then obj
        end
    end
    protected :wrap
end
