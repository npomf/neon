require_relative '../ext/pointers'
require_relative '../opengl/glfw3'

module Gamepad
    extend self

    # Return list of all connected joysticks.
    def connected()
        (GLFW::GLFW_JOYSTICK_LAST + 1).times.map {|n|
            GLFW.glfwJoystickPresent( GLFW.const_get("GLFW_JOYSTICK_#{n}") ) ? n : nil
        }.select {|j| not j.nil? }
    end

    # Get the status of all connected joysticks.
    def all()
        connected.map {|n| stick( n ) }
    end

    # Get information on the given joystick.
    def stick( index )
        {
            name: GLFW.glfwGetJoystickName( index ),
            axes: GLFW.glfwGetJoystickAxes( index, [0].pointer ).read( :float ),
            buttons: GLFW.glfwGetJoystickButtons( index, [0].pointer ).read( :int )
        }
    end
end
