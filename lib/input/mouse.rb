require_relative 'input'
require_relative '../opengl/glfw3'

# Mouse input device wrapper.  Acceleration is the base increase to the distance travelled by the mouse as a percentage.
# A value of 1.0, or 100%, is normal.  Lower values will "slow" the mouse, while higher values will make it seem to have
# moved further.
#
# Axis acceleration, aka "sensitivity," is performed additively with the base acceleration.  Therefore, if the mouse has
# a 10% acceleration (`acceleration` = 1.1) and a Y-axis sensitivity of 40% (`y_senitivity` = 1.4), then the actual
# acceleration of the mouse in the Y-axis is +50%.
class Mouse < Input
    
    # Scroll event hook
    SCROLL_EVENT = SCROLL = :scroll
    # Move event hook
    MOVE_EVENT = MOVE = :move
    # Delta mode; useful for first-person and similar aiming
    DELTA = :delta
    # Absolute mode; best for a UI
    ABSOLUTE = :absolute

    # Mouse acceleration (1.0 = 100% = normal)
    attr_accessor :acceleration
    # Horizontal look sensitivity (axis acceleration)
    attr_accessor :x_sensitivity
    # Vertical look sensitivity (axis acceleration)
    attr_accessor :y_sensitivity
    # Movement mode
    attr_accessor :mode
    # Last X
    attr :x
    # Last Y
    attr :y

    # Initialize.
    def initialize( mode = DELTA, acceleration: 1.0, x_sensitivity: 1.0, y_sensitivity: 1.0 )
        @acceleration = acceleration
        @x_sensitivity = x_sensitivity
        @y_sensitivity = y_sensitivity
        @mode = mode
        reset
    end

    # Reset the mouse's internal coordinates.
    def reset( x = 0, y = 0 )
        @x = x
        @y = y
    end
    
    # Press a button.
    def press( button, mods = 0 )
        button( button, GLFW::GLFW_PRESS, mods )
    end
    
    # Release a button.
    def release( button, mods = 0 )
        button( button, GLFW::GLFW_RELEASE, mods )
    end

    # Handle movement.
    def move( x, y )
        dx, dy = (@mode.eql? DELTA) ? [x - @x, y - @y] : [x, y]
        @x, @y = x, y if @mode.eql? DELTA
        fire( 
            MOVE_EVENT,
            @mode, 
            dx * (@acceleration + (@x_sensitivity - 1.0)), 
            dy * (@acceleration + (@y_sensitivity - 1.0))
        )
    end
    
    # Scroll.  This allows for scrolling in two dimensions, such as via a track-pad.
    def scroll( x, y )
        fire( SCROLL_EVENT, x, y )
    end

    alias :button :fire
end
