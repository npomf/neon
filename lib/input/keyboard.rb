require_relative 'input'
require_relative '../opengl/glfw3'

# Keyboard device wrapper.
class Keyboard < Input
    
    # Press a key.
    def press( key, code = nil, mods = 0 )
        key( key, GLFW::GLFW_PRESS, code, mods )
    end

    
    # Release a key.
    def release( key, code = nil, mods = 0 )
        key( key, GLFW::GLFW_RELEASE, code, mods )
    end
    
    # Repeat a key.
    def repeat( key, code = nil, mods = 0 )
        key( key, GLFW::GLFW_REPEAT, code, mods )
    end

    alias :key :fire
end
