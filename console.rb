require 'readline'
require_relative 'lib/ext/safely'

# Development console.
class Console < Thread
    
    # Internal evaluator of code
    attr :evaluator
    # Sandbox bindings
    attr :bindings
    # Evaluation buffer
    attr :buffer
    # Indicates block level
    attr :block
    
    # Codeblock syntax
	CODEBLOCK = /(do|{)\s?(\|[,\w\s\(\)]+\|)?$/
	# Structure keywords
	STRUCTURE = /^(module|class|def|while|until|for|if|else|elsif|unless|case|begin|rescue|ensure)/
	# End of block syntax
	END_BLOCK = /(end|})$/
	# Inline block syntax
	INLINE_BLOCK = /[\w\d\s\.\?\(\)\\\/_'"-]+\s?\{(\|[,\w\s\(\)]+\|)?.*\}$/
    
    # Initialize a new console session.
    def initialize( sandbox: true )
        @evaluator = (sandbox.is_a? Proc) ? sandbox : method( :sandbox )
        @buffer = ''
        @block = 0
        super(){ runtime }
    end
    
    # Console runtime.
    def runtime()
        puts 'Development console session started.  Terminate with "close".'
        while line = Readline.readline( (@block > 0 ? ' : ' : '$> ') + ('  ' * @block), true )
            break if line =~ /^close$/i
            
            # Handle blocks
            @block += 1 if line =~ CODEBLOCK or line =~ STRUCTURE
            @buffer << line + "\n"
            @block -= 1 if line =~ END_BLOCK and not line =~ INLINE_BLOCK
            
            # Continue until input ends the final evaluable block
            next unless @block == 0 and not @buffer.empty?
            
            # Evaluate
            out = @evaluator.call( @buffer )
            puts (out.is_a? Exception) ? "#{out.class.name}: #{out}" : '-> ' + out.inspect
            @buffer.clear
        end
    end
    protected :runtime
    
    # Sandbox evaluator.
    def sandbox( rb )
        @bindings = binding if @bindings.nil?
        safely { @bindings.eval( rb ) }
    end
end

if __FILE__.eql? $0
    term = Console.new()
    sleep 1.0 while term.status
end
